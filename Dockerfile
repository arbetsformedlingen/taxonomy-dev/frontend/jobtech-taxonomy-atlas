FROM node:22.14.0-alpine3.20 AS builder

COPY package*  webpack* /app/
COPY src/  /app/src/
COPY config.js  /app/config.js
COPY build_finalizer.js  /app/build_finalizer.js

WORKDIR /app

# this part just makes sure there arent any compile errors
RUN apk update &&\
    npm install && npm run release

FROM nginx:1.27.0-alpine3.19-slim

COPY --from=builder /app/target/ /usr/share/nginx/html/
COPY --from=builder /app/target/index.html  /usr/share/nginx/html/index.html
COPY default.conf /etc/nginx/conf.d
COPY nginx.conf /etc/nginx/
COPY config.js  /mnt/volumemount/config.js
RUN apk update &&\
    ln -s /mnt/volumemount/config.js /usr/share/nginx/html/config.js &&\
    chgrp -R 0 /etc/nginx  && chmod -R g=u /etc/nginx &&\
    chgrp -R 0 /var/cache/nginx  && chmod -R g=u /var/cache/nginx &&\
    chgrp -R 0 /var/log/nginx  && chmod -R g=u /var/log/nginx
#RUN chgrp -R 0 /var/run && chmod -R g=u /var/run

#EXPOSE 8000
#CMD npm run  build
