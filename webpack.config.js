var path = require('path');
var webpack = require('webpack');
var TerserPlugin = require('terser-webpack-plugin');

module.exports = {
	entry: {
		index: './src/index.jsx',
		explore: './src/page/explore.jsx',
		taxonomy: './src/page/taxonomy.jsx',
		version: './src/page/version.jsx',
		feedback_form: './src/page/feedback_form.jsx',
		example: './src/page/example.jsx',
		about: './src/page/about.jsx',
	},
	output: { 
		path: __dirname, 
		filename: './src/js/[name].js' 
	},
	module: {
		rules: [
			{
				test: /.jsx?$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['@babel/preset-env', '@babel/preset-react']
					}
				},				
			},
			{
				test: /.css$/,
				use: 'css-loader'
			}
		]
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
    devServer: {
		static: {
			directory: __dirname,
		},
		hot: true,
		host: "0.0.0.0",
		port: 8000,
		allowedHosts: "all"
    },	
	optimization: {
		minimize: true,
		minimizer: [new TerserPlugin({
			parallel: true,
			terserOptions: {
				ecma: 6,
			},
		})],
	},
};
