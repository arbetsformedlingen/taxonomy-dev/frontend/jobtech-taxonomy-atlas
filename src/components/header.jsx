import React from 'react';
import ReactDOM from 'react-dom';
import AutocompleteSearch from './autocomplete_search.jsx';
import Constants from './../context/constants.jsx';
import Localization from './../context/localization.jsx';
import Excel from './../context/excel.jsx';
import Rest from './../context/rest.jsx';
import Util from './../context/util.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Content from '../page/taxonomy/content.jsx';

class Header extends React.Component { 

    constructor() {
        super();
        this.state = {
            isShowingExport: false,
            isShowingLinks: false,
            isShowingAbout: false,
        };
    }

    componentDidMount() {
        document.addEventListener("mousedown", this.onMouseDown.bind(this), false);
        // NOTE: dont use this just yet, want to give admins time to update values first
        /*Rest.getTypesLocalization((data) => {
            for(var i=0; i<data.length; ++i) {
                var value = data[i];
                Localization.resources.sv["db_" + value.id] = value.label_sv;
                Localization.resources.en["db_" + value.id] = value.label_en;
            }
            EventDispatcher.fire(Constants.EVENT_LANGUAGE_CHANGED);
        });*/
    }

    onMouseDown(e) {
        if(this.state.isShowingExport || this.state.isShowingAbout || this.state.isShowingLinks) {
            this.setState({
                isShowingExport: false,
                isShowingLinks: false,
                isShowingAbout: false,
            });
        }
    }

    onLinkClicked(url) {
        var isSame = window.location.pathname == url;
        window.location.pathname = url;
        if(isSame) {
            location.reload();
        }
    }

    onTitleClicked() {
		Constants.replaceArg("concept", null);
        this.onLinkClicked("/page/taxonomy.html");
    }

    onExportClicked() {
        this.setState({isShowingExport: true});
    }

    onLinksClicked() {
        this.setState({isShowingLinks: true});
    }

    onAboutClicked() {
        this.setState({isShowingAbout: true});
        //window.open("https://gitlab.com/team-batfish/jobtech-taxonomy-api/-/blob/develop/REFERENCE-svenska.md", '_blank');
    }

    onLanguageClicked(lang) {
        Localization.changeLanguage(lang);
        EventDispatcher.fire(Constants.EVENT_LANGUAGE_CHANGED);
        this.forceUpdate();
    }

    onAutocompleteSelected(item) {
        if(window.location.pathname.indexOf("taxonomy.html") != -1) {
            Rest.getConcept(item.id, null, (data) => {
                EventDispatcher.fire(Constants.EVENT_AUTOCOMPLETE_ITEM, data[0]);
            });
        } else {
            Constants.replaceArg("concept", item.id);
            this.onLinkClicked("/page/taxonomy.html");
        }
    }

    async onExportOccupationsClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_POPUP_INDICATOR, Localization.get("exporting") + "...");
		var versionName = Constants.getArg("v");
        var version = versionName != null ? ", version: \"" + versionName + "\"" : "";
        var query = 
            "query Atlas {" +
                "concepts(type: \"occupation-field\"" + version + ") {" +
                    "label: preferred_label " +
                    "children: narrower(type: \"ssyk-level-4\") {" +
                        "label: preferred_label " +
                        "code: ssyk_code_2012 " +
                        "children: narrower(type: \"occupation-name\") {" +
                            "label: preferred_label " +
                        "}" +
                    "}" +
                "}" +
            "}";
        var structure = await Rest.awaitGraphQl(query);
        if(structure != null) {
            var data = structure.data.concepts;
            Util.sortByKey(data, "label", true);
            var vs = await Rest.getVersionsPromise();
            version = versionName == null ? vs[vs.length - 1].version : versionName;
            // generate excel
            var columns = [{
                text: "Yrkesområde",
                width: 30,
            }, {
                text: "SSYK 2012",
                width: 20,
            }, {
                text: "Yrkesgrupp",
                width: 55,
            }, {
                text: "Yrkesbenämning",
                width: 40,
            }];
            var name = "Yrkesgrupper";
            var context = Excel.createSimple(name, version, columns);
            for(var i=0; i<data.length; ++i) {
                var field = data[i];
                Util.sortByKey(field.children, "code", true);
                for(var j=0; j<field.children.length; ++j) {
                    var group = field.children[j];
                    Util.sortByKey(group.children, "label", true);
                    for(var k=0; k<group.children.length; ++k) {
                        var occupation = group.children[k];
                        context.addRow(["", field.label, group.code, group.label, occupation.label]);
                    }
                }
            }
            context.download(name + ".xlsx");
        }
        EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
    }

    async onExportSkillsClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_POPUP_INDICATOR, Localization.get("exporting") + "...");
		var versionName = Constants.getArg("v");
        var version = versionName != null ? ", version: \"" + versionName + "\"" : "";
        var query = 
            "query Atlas {" +
                "concepts(type: \"skill\"" + version + ") {" +
                    "label: preferred_label " +
                    "parent: broader(type: \"skill-headline\") {" +
                        "id " +
                        "label: preferred_label " +
                    "}" +
                    "children: related(type: \"ssyk-level-4\") {" +
                        "label: preferred_label " +
                        "code: ssyk_code_2012 " +
                    "}" +
                "}" +
            "}";
        var structure = await Rest.awaitGraphQl(query);
        if(structure != null) {
            var data = structure.data.concepts;
            Util.sortByKey(data, "label", true);
            var vs = await Rest.getVersionsPromise();
            version = versionName == null ? vs[vs.length - 1].version : versionName;
            // generate excel
            var name = "Kompetenser";
            var context = Excel.create(name, name, version);
            context.addRow();
            // insert data
            var prevParentId = null;
            for(var i=0; i<data.length; ++i) {
                var item = data[i];
                item.parent = item.parent[0];
                Util.sortByKey(item.children, "code", true);
                if(prevParentId != item.parent.id) {
                    prevParentId = item.parent.id;
                    context.addRow();
                    context.addGroupRow(item.parent.label, null, null, true);
                }
                if(item.children.length) {
                    context.addGroupRow(item.label, item.children[0].code, item.children[0].label);
                    for(var j=1; j<item.children.length; ++j) {
                        context.addGroupRow(null, item.children[j].code, item.children[j].label);
                    }
                } else {
                    context.addGroupRow(item.label);
                }
            }
            context.download(name + ".xlsx");
        }
        EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
    }

    async onExportSsykClicked() {
        EventDispatcher.fire(Constants.EVENT_SHOW_POPUP_INDICATOR, Localization.get("exporting") + "...");
		var versionName = Constants.getArg("v");
        var version = versionName != null ? ", version: \"" + versionName + "\"" : "";
		var query = 
            "query Atlas {" +
                "concepts(type: \"occupation-field\"" + version + ") {" +
                    "label: preferred_label " +
                    "children: narrower(type: \"ssyk-level-4\") {" +
                        "label: preferred_label " +
                        "code: ssyk_code_2012 " +
                    "}" +
                "}" +
            "}";
        var structure = await Rest.awaitGraphQl(query);
        if(structure != null) {
            var data = structure.data.concepts;
            Util.sortByKey(data, "label", true);
            var vs = await Rest.getVersionsPromise();
            version = versionName == null ? vs[vs.length - 1].version : versionName;
            // generate excel
            var name = "Yrken";
            var context = Excel.create(name, name, version);
            context.addRow();
            // insert data
            for(var i=0; i<data.length; ++i) {
                var item = data[i];
                Util.sortByKey(item.children, "code", true);
                if(item.children.length) {
                    context.addRow();
                    context.addGroupRow(item.label, item.children[0].code, item.children[0].label);
                    for(var j=1; j<item.children.length; ++j) {
                        context.addGroupRow(null, item.children[j].code, item.children[j].label);
                    }
                } else {
                    context.addRow();
                    context.addGroupRow(item.label);
                }
            }
            context.download(name + ".xlsx");
        }
        EventDispatcher.fire(Constants.EVENT_HIDE_POPUP_INDICATOR);
    }

    renderTitle() {
        return (
            <div 
                alt={Localization.get("alt_homepage")}
                className="header_title"
                onPointerUp={this.onTitleClicked.bind(this)}>
                <div>
                    <div>JobTech</div>
                    <div>Atlas</div>
                </div>
                <div>
                    {Localization.get("sub_title")}
                </div>
            </div>
        );
    }

    renderExportMenu() {
        if(this.state.isShowingExport) {
            return (
                <ul className="export_menu">
                    <li onPointerDown={this.onExportOccupationsClicked.bind(this)}>
                        Yrken - yrkesgrupper - yrkesområden
                    </li>
                    <li onPointerDown={this.onExportSkillsClicked.bind(this)}>
                        Kompetenser - kompetensgrupper-yrkesgrupper
                    </li>
                </ul>
            );
        }
    }

    renderLinksMenu() {
        if(this.state.isShowingLinks) {
            return (
                <ul className="export_menu">
                    <li onPointerUp={() => window.open("https://arbetsformedlingen.se/for-arbetssokande/yrken-och-studier/hitta-yrken/", '_blank')}>
                        Hitta yrken
                    </li>
                    <li onPointerUp={() => window.open("https://arbetsformedlingen.se/for-arbetssokande/yrken-och-studier/yrkeskompassen/#/", '_blank')}>
                        Yrkeskompassen
                    </li>
                </ul>
            );
        }
    }

    renderAbout() {
        if(this.state.isShowingAbout) {
            return (
                <div className="about_content">
                    <div>
                        {Localization.get("about_content")}
                    </div>
                </div>
            );
        }
    }

    renderMenu() {
        /*
                <div>
                    <div
                        id="export_button" 
                        className="header_export"
                        onPointerUp={this.onLinksClicked.bind(this)}>
                        <i className="down"/>
                        <div>{Localization.get("links")}</div>
                    </div>
                    {this.renderLinksMenu()}
                </div>
                    {this.renderAbout()}
        */
        return (
            <div className="header_menu">
                <div>
                    <div
                        id="export_button" 
                        className="header_export"
                        onPointerUp={this.onExportClicked.bind(this)}>
                        <i className="down"/>
                        <div>{Localization.get("export")}</div>
                    </div>
                    {this.renderExportMenu()}
                </div>
                <div onPointerUp={this.onLinkClicked.bind(this, "/page/version.html")}>
                    {Localization.get("versions")}
                </div>
                <div onPointerUp={this.onLinkClicked.bind(this, "/page/explore.html")}>
                    {Localization.get("explore")}
                </div>
                <div onPointerUp={this.onLinkClicked.bind(this, "/page/about.html")}>
                    {Localization.get("about")}
                </div>
            </div>
        );
    }

    renderLanguage() {
        var langOption = (text, lang) => {
            return (
                <div 
                    className={"header_language_option " + (Localization.key == lang ? "header_language_selected" : "")}
                    onPointerUp={this.onLanguageClicked.bind(this, lang)}>
                    {text}
                </div>
            );
        };
        return (
            <div className="header_language_container">
                <div>
                    {Localization.get("language")}:
                </div>
                {langOption("Svenska", "sv")}
                {langOption("English", "en")}
            </div>
        );
    }

    render() {
        var version = Constants.getArg("v");
        var placeholder = version == null ? Localization.get("autocomplete_placeholder") : Localization.get("autocomplete_placeholder_version") + " " + version;
        return (
            <div className="header_content font">
                {this.renderTitle()}
                <AutocompleteSearch 
					placeholder={placeholder}
					callback={this.onAutocompleteSelected.bind(this)}/>
                <div className="header_menu_container">
                    {this.renderMenu()}
                    <div className="header_divider"/>
                    {this.renderLanguage()}
                </div>
            </div>
        );
    }
}

export default Header;