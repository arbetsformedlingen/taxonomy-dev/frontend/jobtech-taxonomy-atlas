import React from 'react';
import ReactDOM from 'react-dom';
import Loader from './loader.jsx';

class TreeViewItem extends React.Component { 
	
	constructor(props) {
		super(props);
		this.state = {
            depth: props.depth,
            item: props.item,
            isExpanded: props.item.isExpanded,
            isSelected: false,
            isHighlighted: false,
        };
        this.callback = props.callback;
        props.item.setSelected = this.setSelected.bind(this);
        props.item.setHighlighted = this.setHighlighted.bind(this);
        props.item.setExpanded = this.setExpanded.bind(this);
    }
    
    setSelected(isSelected) {
        this.setState({isSelected: isSelected});
    }

    setHighlighted(isHighlighted) {
        this.setState({isHighlighted: isHighlighted});
    }
    
    setExpanded(isExpanded) {
        this.setState({isExpanded: isExpanded});
    }

    onItemClicked() {
        if(this.callback) {
            this.callback(this.state.item);
        }
    }
    
	renderChildren(item, depth) {
		if(item.children && item.children.length && this.state.isExpanded) {
			var items = item.children.map((element, index) => {
				if(element.isVisible == null || element.isVisible) {
                    return (
                        <TreeViewItem 
                            item={element} 
                            depth={depth} 
                            callback={this.callback}
                            highlight={this.props.highlight}
                            key={index}/>
                    );
                }
			});
			items = items.filter(Boolean);
			return (
				<ul className="tree_item_children">
					{items}
				</ul>
			);
		}
	}

	renderIcon(item) {
		if(item.children && item.children.length) {
			return ( <i className={item.isExpanded ? "down" : "right"}/> );
		} else {
			return ( <i className="empty"/> );
		}
	}

	renderText(item) {
		if(item.ssyk_code && item.type && item.type.startsWith("ssyk")) {
			return ( <div>{item.ssyk_code.code} - {item.label}</div> );
		} else if(item.isco_code && item.type && item.type.startsWith("isco")) {
			return ( <div>{item.isco_code.code} - {item.label}</div> );
		} else if(item.code) {
			return ( <div>{item.code} - {item.label}</div> );
		} else if(item.children == null || item.children.length == 0) {
			return ( <div>- {item.label}</div> );
		} else {
			return ( <div>{item.label}</div> );
		}
	}
	
	render() {
        var item = this.state.item;
		var css = this.state.isHighlighted ? "tree_item_selected" : "";
        if(this.state.isSelected) {
            css += " tree_item_selected_root";
        }
        if(this.props.highlight == false) {
            css = "";
        }
        return (
            <li className="tree_item_base">
                <div
                    id={item.id}
                    className={"tree_item rounded tree_item_" + this.state.depth + " font " + css}
                    onPointerUp={this.onItemClicked.bind(this)}>
                    {this.renderIcon(item)}
                    {this.renderText(item)}
                </div>
                {this.renderChildren(item, this.state.depth + 1)}
            </li>
        );
	}
};

export default TreeViewItem;