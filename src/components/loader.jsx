import React from 'react';
import Localization from './../context/localization.jsx';

class Loader extends React.Component { 

    constructor() {
        super();
        this.text = Localization.get("loading") + "...";
    }

    render() {
        var text = this.props.text != null ? this.props.text : this.text;
        return (
            <div className="loader_group font">
                <div className="loader"/>
                <div>{text}</div>
            </div>
        );
    }
	
}

export default Loader;