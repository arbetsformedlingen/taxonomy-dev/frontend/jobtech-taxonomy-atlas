import React from 'react';
import ReactDOM from 'react-dom';
import Loader from './loader.jsx';
import TreeViewItem from './treeview_item.jsx';
import Util from './../context/util.jsx';

class TreeView extends React.Component { 
	
	constructor(props) {
		super(props);
		this.css = props.css;
		this.state = {
			isLoading: false,
			hadFilter: false,
			hasFilter: false,
			roots: props.roots ? props.roots : [],
			preSelectObject: null,
		};
		this.selected = null;
		var f = props.filter != null ? props.filter.normalize('NFD').replace(/[\u0300-\u036f]/g, "") : null;
		this.setupParentRelations(this.state.roots);
		this.filterItems(this.state.roots, f != null ? f.toLowerCase() : null);
	}

	UNSAFE_componentWillReceiveProps(props) {
		if(!this.isRootsChanged(props.roots) &&
			!this.isLoadingChanged(props.isLoading) &&
			!this.isFilterChanged(props.filter) &&
			!this.isPreSelectChanged(props.preSelectObject)) {
			return;
		}
		if(props.roots) {
			this.setupParentRelations(props.roots);
			if(this.prevFilter != props.filter) {
				var f = props.filter != null ? props.filter.normalize('NFD').replace(/[\u0300-\u036f]/g, "") : null;
				this.filterItems(props.roots, f != null ? f.toLowerCase() : null);
				this.prevFilter = props.filter;
				this.forceUpdate();
			}
			this.setState({
				isLoading: props.isLoading != null ? props.isLoading : this.state.isLoading,
				hadFilter: this.state.hasFilter,
				hasFilter: props.filter != null && props.filter.length > 0,
				roots: props.roots,
				preSelectObject: props.preSelectObject,
			}, () => {
				if(!this.state.isLoading) {
					var selected = this.preSelectItem(props.roots, props.preSelectCallback);
					if(selected) {
						var parent = selected.parent;
						var lastParent = null;
						while(parent) {
							lastParent = parent;
							if(parent.isExpanded != null) {
								parent.isExpanded = true;
							}
							parent = parent.parent;
						}
						selected.scroll = true;
						if(lastParent && lastParent.setExpanded) {
							lastParent.setExpanded(true);
						}
						this.onTrySelect(selected);
					}
				}
			});
		} else {
			this.setState({
				isLoading: false,
				roots: [],
				preSelectObject: null,
			});
		}
	}

	componentDidUpdate() {
		if(this.selected && this.selected.scroll) {
			this.scrollToId(this.selected.id, 30);
		}
	}

	isFilterChanged(filter) {
		return this.prevFilter != filter;
	}

	isLoadingChanged(isLoading) {
		return this.state.isLoading != isLoading;
	}

	isPreSelectChanged(preSelectObject) {
		//return (this.state.preSelectObject != null && preSelectObject != null) && 
		return this.state.preSelectObject != preSelectObject;
	}

	isRootsChanged(roots) {
		if(roots.length != this.state.roots.length) {
			return true;
		}
		for(var i=0; i<roots.length; ++i) {
			if(roots[i].label != this.state.roots[i].label) {
				return true;
			}
		}
		return false;
	}

	scrollToId(id, count) {
		var element = document.getElementById(id);
		if(element != null) {
			element.scrollIntoView({
				behavior: "auto", 
				block: "center", 
				inline: "start"
			});
			this.selected.scroll = null;
		} else if(count >= 0) {
			setTimeout(() => {
				this.scrollToId(id, count - 1);
			}, 1000);
		} else {
			this.selected.scroll = null;
		}
	}

	onTrySelect(item) {
		if(item.setSelected) {
			this.onItemClicked(item);
		} else {
			setTimeout(this.onTrySelect.bind(this, item), 50);
		}
	}

	setupParentRelations(roots, parent) {
		var sortable = roots.filter((x) => { return x.sortOrder != null; });
		if(sortable.length > 0) {
			var remainder = roots.filter((x) => { return x.sortOrder == null; });
			Util.sortByKey(sortable, "sortOrder", true);
			// restore array with sorted values		
			roots.length = 0;
			roots.push(...sortable);
			roots.push(...remainder);
		}
		for(var i=0; i<roots.length; ++i) {
			var node = roots[i];
			if(!node.isTreeReady && (node.parent != parent || parent == null)) {
				node.parent = parent;
				node.isTreeReady = true;
				if(node.children) {
					for(var j=0; j<node.children.length; ++j) {
						this.setupParentRelations(node.children, node);
					}
				}
			}
		}
	}

	preSelectItem(items, callback) {
		if(callback) {
			for(var i=0; i<items.length; ++i) {
				var item = items[i];
				if(callback(item)) {
					return item;
				}
				if(item.children) {
					var r = this.preSelectItem(item.children, callback);
					if(r) {
						return r;
					}
				}
			}
		}
		return null;
	}

	filterItems(items, query) {
		var any = false;
		for(var i=0; i<items.length; ++i) {
			var item = items[i];
			if(item.filterCheckName == null) {
				item.filterCheckName = item.label.normalize('NFD').replace(/[\u0300-\u036f]/g, "");
				item.filterCheckName = item.filterCheckName.toLowerCase();
				if(item.ssyk_code || item.isco_code || item.code) {
					var code = "";
					if(item.ssyk_code) {
						code = item.ssyk_code.code;
					} else if(item.isco_code) {
						code = item.isco_code.code;
					} else {
						code = item.code;
					}
					item.filterCheckName = code + " - " + item.filterCheckName;
				}
			}
			item.isVisible = (query == null || query == "") ? true : item.filterCheckName.indexOf(query) != -1;
			if(item.children) {
				if(this.filterItems(item.children, query)) {
					item.isVisible = true;
				}
			}
			if(item.isVisible) {
				any = true;
			}
		}
		return any;
	}

	setHighlightedChain(root, value) {
		if(this.props.highlight || this.props.highlight == null) {
			var p = root;
			while(p != null) {
				if(p.setHighlighted != null) {
					p.setHighlighted(value);
				}
				p = p.parent;
			}
		}
	}

	onItemClicked(item) {
		if(this.selected) {
			this.setHighlightedChain(this.selected.parent, false);
			this.selected.setSelected(false);
		}
		this.selected = item;
		if(this.props.onClick) {
			this.props.onClick(item);
		} else if(item.isExpanded != null) {
			item.isExpanded = !item.isExpanded;
		}
		this.setHighlightedChain(item.parent, true);
		item.setSelected(true);
		item.setExpanded(item.isExpanded);
	}
	
	render() {
		var items = [];
		if(this.state.isLoading) {
			items = <Loader />;
		} else {
			items = this.state.roots.map((element, index) => {
				if(element.isVisible == null || element.isVisible) {
					return ( 
						<TreeViewItem 
							item={element} 
							key={element.label + element.id}
							depth={0} 
							callback={this.onItemClicked.bind(this)}
							highlight={this.props.highlight}/>
					);
				}
			});
			items = items.filter(Boolean);
		}
		var css = this.css ? "tree_view " + this.css : "tree_view";
		return (
			<ul className={css}>
				{items}
			</ul>
		);
	}
};

export default TreeView;