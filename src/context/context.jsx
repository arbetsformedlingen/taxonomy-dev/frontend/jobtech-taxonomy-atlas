import React from 'react';
import LZString from 'lz-string';
import TaxUtil from './../page/taxonomy/util.jsx';
import Util from './util.jsx';

class Context {

    constructor() {
        this.escoOccuptionItems = this.getCacheValue("atlas_esco_occupation_cache");
        this.escoSkillItems = this.getCacheValue("atlas_esco_skill_cache");
        this.skillItems = this.getCacheValue("atlas_skill_cache");
        this.isLoadingEscoOccupation = this.escoOccuptionItems == null;
        this.isLoadingEscoSkills = this.escoSkillItems == null;
        this.isLoadingSkills = this.skillItems == null;
        this.escoLoadCompleteCallback = null;
        this.skillLoadCompleteCallback = null;
    }

    setCacheValue(key, value) {
        localStorage.setItem(key + "_date", Date.now());
        localStorage.setItem(key, LZString.compress(JSON.stringify(value)));
    }

    getCacheValue(key) {
        var date = localStorage.getItem(key + "_date");
        if(date != null && (Date.now() - parseInt(date)) < 12 * 60 * 60 * 1000) {
            var value = localStorage.getItem(key);
            try {
                value = LZString.decompress(value);
                return JSON.parse(value);
            } catch(e) {
                return null;
            }
        }
        return null;
    }

    async fetchSkill() {
        this.isLoadingSkills = true;
        this.skillItems = await TaxUtil.getSkills();
        this.isLoadingSkills = false;
        if(this.skillItems != null) {
            this.setCacheValue("atlas_skill_cache", this.skillItems);
        }
        if(this.skillLoadCompleteCallback) {
            this.skillLoadCompleteCallback(this.skillItems);
        }
    }

    async fetchEscoOccupation() {
        this.isLoadingEscoOccupation = true;
        this.escoOccuptionItems = await TaxUtil.getEscoOccupation();
        this.isLoadingEscoOccupation = false;
        if(this.escoOccuptionItems != null) {
            this.setCacheValue("atlas_esco_occupation_cache", this.escoOccuptionItems);
        }
        if(this.escoLoadCompleteCallback) {
            this.escoLoadCompleteCallback(this.escoOccuptionItems, true);
        }
    }

    async fetchEscoSkill() {
        this.isLoadingEscoSkills = true;
        this.escoSkillItems = await TaxUtil.getEscoSkill();
        this.isLoadingEscoSkills = false;
        if(this.escoSkillItems != null) {
            this.setCacheValue("atlas_esco_skill_cache", this.escoSkillItems);
        }
        if(this.escoLoadCompleteCallback) {
            this.escoLoadCompleteCallback(this.escoSkillItems, false);
        }
    }

    async init() {
        if(this.skillItems == null) {
            await this.fetchSkill();
        }
        if(this.escoOccuptionItems == null) {
            await this.fetchEscoOccupation();
        }
        if(this.escoSkillItems == null) {
            await this.fetchEscoSkill();
        }
    }

}

export default new Context;