import React from 'react';

class Constants { 

    constructor() {
        // settings
        this.REST_IP = window.g_app_api_path;
        this.REST_API_KEY = window.g_app_api_key;

        // display work type
        this.DISPLAY_WORK_TYPE_STRUCTURE = 0;
        this.DISPLAY_WORK_TYPE_GROUP = 1;
        this.DISPLAY_WORK_TYPE_OCCUPATIONS = 2;
        this.DISPLAY_WORK_TYPE_FIELDS = 3;

        // feedback type
        this.FEEDBACK_TYPE_WORK = 0;
        this.FEEDBACK_TYPE_SKILL = 1;
        this.FEEDBACK_TYPE_OTHER = 2;

        // display type
        this.DISPLAY_TYPE_WORK_SSYK = "work_ssyk";
        this.DISPLAY_TYPE_WORK_ISCO = "work_isco";
        this.DISPLAY_TYPE_SKILL = "skill";
        this.DISPLAY_TYPE_SKILL_COLLECTION = "skill_collection";
        this.DISPLAY_TYPE_SWE_SKILL = "swe_skill";
        this.DISPLAY_TYPE_OTHER = "other";
        this.DISPLAY_TYPE_WORK_DESC = "work_desc";
        this.DISPLAY_TYPE_GEOGRAPHY = "geography";
        this.DISPLAY_TYPE_INDUSTRY = "industry";
        this.DISPLAY_TYPE_SEARCH = "search";
        this.DISPLAY_TYPE_LANGUAGE = "language";
        this.DISPLAY_TYPE_EDUCATION = "education";
        this.DISPLAY_TYPE_ESCO_OCCUPATION = "esco_occupation";
        this.DISPLAY_TYPE_ESCO_SKILL = "esco_skill";
        this.DISPLAY_TYPE_FORECAST_OCCUPATION = "forecast_occupation";
        this.DISPLAY_TYPE_BAROMETER_OCCUPATION = "barometer_occupation";
        this.DISPLAY_TYPE_JOB_TITLE = "job_title";
        this.DISPLAY_TYPE_GENERIC_SKILL = "generic_skill";

        // relation types
        this.RELATION_NARROWER = "narrower";
        this.RELATION_BROADER = "broader";
        this.RELATION_RELATED = "related";

        // resources
        this.ICON_JOBTECH_BLACK = "./../resources/JobTechDevelopment_black.png";
        this.ICON_JOBTECH_WHITE = "./../resources/JobTechDevelopment_white.png";
        this.ICON_AF_EXPORT = "./../resources/av_logo.png";
        this.ICON_AF_EXPORT_B64 = null;

        // svg constants
        this.FACEBOOK_ICON = <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1343 12v264h-157q-86 0-116 36t-30 108v189h293l-39 296h-254v759h-306v-759h-255v-296h255v-218q0-186 104-288.5t277-102.5q147 0 228 12z"></path></svg>;
        this.TWITTER_ICON = <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M1684 408q-67 98-162 167 1 14 1 42 0 130-38 259.5t-115.5 248.5-184.5 210.5-258 146-323 54.5q-271 0-496-145 35 4 78 4 225 0 401-138-105-2-188-64.5t-114-159.5q33 5 61 5 43 0 85-11-112-23-185.5-111.5t-73.5-205.5v-4q68 38 146 41-66-44-105-115t-39-154q0-88 44-163 121 149 294.5 238.5t371.5 99.5q-8-38-8-74 0-134 94.5-228.5t228.5-94.5q140 0 236 102 109-21 205-78-37 115-142 178 93-10 186-50z"></path></svg>;
        this.LINKEDIN_ICON = <svg viewBox="0 0 1792 1792" xmlns="http://www.w3.org/2000/svg"><path d="M477 625v991h-330v-991h330zm21-306q1 73-50.5 122t-135.5 49h-2q-82 0-132-49t-50-122q0-74 51.5-122.5t134.5-48.5 133 48.5 51 122.5zm1166 729v568h-329v-530q0-105-40.5-164.5t-126.5-59.5q-63 0-105.5 34.5t-63.5 85.5q-11 30-11 81v553h-329q2-399 2-647t-1-296l-1-48h329v144h-2q20-32 41-56t56.5-52 87-43.5 114.5-15.5q171 0 275 113.5t104 332.5z"></path></svg>;
        this.GITHUB_ICON = <svg viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27.68 0 1.36.09 2 .27 1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.013 8.013 0 0016 8c0-4.42-3.58-8-8-8z"></path></svg>;
        this.EMAIL_ICON = <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" width="22" height="22"><path d="m 0 3.05 l 0 18 l 24 0 l 0 -18 l -24 0 Z m 6.623 8.45 l -5.073 6 l 0 -10.95 l 5.073 4.95 Z m -4.573 -6.95 l 20 0 l -10.05 9.45 l -9.95 -9.45 Z m 5.95 7.95 l 4 3.5 l 4 -3.5 l 6.1 7.05 l -20 0 l 5.9 -7.05 Z m 9.384 -1 l 5.166 -5 l 0 11 l -5.166 -6 Z"/></svg>
        this.FORUM_ICON = <svg viewBox="0 0 980 890" xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path d="M 861.9 482.9 l -101.3 0.7 L 555.7 650 l -42.6 -166.3 l -41 -0.7 c -70.7 0 -128.1 -57.3 -128.1 -128.1 V 188.5 c 0 -70.7 57.3 -128.1 128.1 -128.1 h 389.8 c 70.7 0 128.1 57.3 128.1 128.1 v 166.3 C 990 425.6 932.7 482.9 861.9 482.9 Z M 945 188.6 c 0 -46 -37.5 -83.5 -83.5 -83.5 H 471.7 c -46 0 -83.5 37.5 -83.5 83.5 V 355 c 0 46.1 37.5 83.5 84.3 83.5 l 74.9 1.3 l 35.4 138.4 l 163.7 -138.9 l 114.9 -0.8 c 46 0 83.5 -37.5 83.5 -83.5 V 188.6 L 945 188.6 Z M 54.1 433.6 v 210.9 c 0 46.1 37.5 83.5 84.3 83.5 l 74.9 1.3 l 35.4 138.4 l 141.5 -138.8 l 137.2 -0.8 c 46 0 83.5 -15.2 83.5 -61.2 v -0.5 l 45 -30 v 30.4 c 0 70.7 -57.3 105.8 -128.1 105.8 l -123.5 0.7 L 221.6 939.5 L 179 773.2 l -41 -0.7 C 67.3 772.5 10 715.2 10 644.4 V 433.5 c 0 -70.7 57.3 -128.1 128.1 -128.1 H 299 v 44.7 H 137.6 C 91.6 350.1 54.1 387.6 54.1 433.6 Z"></path></svg>;
        this.GITLAB_ICON = <svg height="24" viewBox="0 0 1131 342" xmlns="http://www.w3.org/2000/svg">
                <g fill="none" fillRule="evenodd">
                <g fill="#fff">
                <path d="M764.367 94.13h-20.803l.066 154.74h84.155v-19.136h-63.352l-.066-135.603zM907.917 221.7c-5.2 5.434-13.946 10.87-25.766 10.87-15.838 0-22.22-7.797-22.22-17.957 0-15.354 10.637-22.678 33.332-22.678 4.255 0 11.11.472 14.655 1.18v28.586zm-21.51-93.787c-16.8 0-32.208 5.952-44.23 15.858l7.352 12.73c8.51-4.962 18.91-9.924 33.802-9.924 17.02 0 24.585 8.742 24.585 23.39v7.56c-3.31-.71-10.164-1.184-14.42-1.184-36.404 0-54.842 12.757-54.842 39.454 0 23.86 14.656 35.908 36.876 35.908 14.97 0 29.314-6.852 34.278-17.954l3.782 15.118h14.657v-79.14c0-25.04-10.874-41.815-41.84-41.815zM995.368 233.277c-7.802 0-14.657-.945-19.858-3.308v-71.58c7.093-5.908 15.84-10.16 26.95-10.16 20.092 0 27.893 14.174 27.893 37.09 0 32.6-12.53 47.957-34.985 47.957m8.742-105.364c-18.592 0-28.6 12.64-28.6 12.64V120.59l-.066-26.458H955.116l.066 150.957c10.164 4.25 24.11 6.613 39.24 6.613 38.768 0 57.442-24.804 57.442-67.564 0-33.783-17.26-56.227-47.754-56.227M538.238 110.904c18.438 0 30.258 6.142 38.06 12.285l8.938-15.477c-12.184-10.678-28.573-16.417-46.053-16.417-44.204 0-75.17 26.932-75.17 81.267 0 56.935 33.407 79.14 71.624 79.14 19.148 0 35.46-4.488 46.096-8.976l-.435-60.832V162.76h-56.734v19.135h36.167l.437 46.184c-4.727 2.362-13 4.252-24.11 4.252-30.73 0-51.297-19.32-51.297-60.006 0-41.34 21.275-61.422 52.478-61.422M684.534 94.13h-20.33l.066 25.988v89.771c0 25.04 10.874 41.814 41.84 41.814 4.28 0 8.465-.39 12.53-1.126v-18.245c-2.943.45-6.083.707-9.455.707-17.02 0-24.585-8.74-24.585-23.387v-61.895h34.04v-17.01H684.6l-.066-36.617zM612.62 248.87h20.33V130.747h-20.33v118.12zM612.62 114.448h20.33V94.13h-20.33v20.318z"></path>
                </g>
                <path d="M185.398 341.13l68.013-209.322H117.39L185.4 341.13z" fill="#E24329"></path>
                <path d="M185.398 341.13l-68.013-209.322h-95.32L185.4 341.128z" fill="#FC6D26" ></path>
                <path d="M22.066 131.808l-20.67 63.61c-1.884 5.803.18 12.16 5.117 15.744L185.398 341.13 22.066 131.807z" fill="#FCA326"></path>
                <path d="M22.066 131.808h95.32L76.42 5.735c-2.107-6.487-11.284-6.487-13.39 0L22.065 131.808z" fill="#E24329" ></path>
                <path d="M185.398 341.13l68.013-209.322h95.32L185.4 341.128z" fill="#FC6D26"></path>
                <path d="M348.73 131.808l20.67 63.61c1.884 5.803-.18 12.16-5.117 15.744L185.398 341.13 348.73 131.807z" fill="#FCA326"></path>
                <path d="M348.73 131.808h-95.32L294.376 5.735c2.108-6.487 11.285-6.487 13.392 0l40.963 126.073z" fill="#E24329"></path>
                </g>
            </svg>;

        // external media sites
        this.FACEBOOK_URL = "https://www.facebook.com/jobtechdevelopment";
        this.TWITTER_URL = "https://twitter.com/jobtechdev?lang=en";
        this.LINKEDIN_URL = "https://www.linkedin.com/company/jobtech-sweden";
        this.GITHUB_URL = "https://github.com/JobtechSwe";
        this.GITLAB_URL = "https://gitlab.com/team-batfish/frontend/jobtech-taxonomy-public";

        // events
        this.EVENT_TAXONOMY_SET_SEARCH_TYPE = "EVENT_TAXONOMY_SET_SEARCH_TYPE";
        this.EVENT_SHOW_POPUP_INDICATOR = "EVENT_SHOW_POPUP_INDICATOR";
        this.EVENT_HIDE_POPUP_INDICATOR = "EVENT_HIDE_POPUP_INDICATOR";
        this.EVENT_LANGUAGE_CHANGED = "EVENT_LANGUAGE_CHANGED";
        this.EVENT_AUTOCOMPLETE_ITEM = "EVENT_AUTOCOMPLETE_ITEM";

        // graphql fragments
        this.GRAPHQL_DEFAULT_ITEM = 
            "id " +
            "type " +
            "uri " +
            "esco_uri " +
            "label: preferred_label " +
            "alternativeLabels: alternative_labels " +
            "sortOrder: sort_order ";
        this.GRAPHQL_SSYK_LIST_TYPE = "ssyk_type";
        this.GRAPHQL_SSYK_LIST_TYPE_ITEM = "..." + this.GRAPHQL_SSYK_LIST_TYPE + " ";
        this.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT =
            " fragment " + this.GRAPHQL_SSYK_LIST_TYPE + " on Concept {" +
				this.GRAPHQL_DEFAULT_ITEM +
				"ssyk_code: ssyk_code_2012 " +
			"}";

        // graphql queries
        this.GRAPHQL_SSYK_STRUCTURE = 
			this.graphQlGenerateQuery(this.graphQlConceptQuery(null, "type: \"ssyk-level-1\"",
				this.GRAPHQL_SSYK_LIST_TYPE_ITEM +
				"children: narrower(type: \"ssyk-level-2\") {" +
                    this.GRAPHQL_SSYK_LIST_TYPE_ITEM +
					"children: narrower(type: \"ssyk-level-3\") {" +
                        this.GRAPHQL_SSYK_LIST_TYPE_ITEM +
						"children: narrower(type: \"ssyk-level-4\") {" +
                            this.GRAPHQL_SSYK_LIST_TYPE_ITEM +
							"children: narrower(type: \"occupation-name\") {" + this.GRAPHQL_DEFAULT_ITEM + "}" +
						"}" +
					"}" +
				"}"
			)) + this.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT;

        this.GRAPHQL_SSYK_GROUPS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery(null, "type: \"ssyk-level-4\"",
                this.GRAPHQL_SSYK_LIST_TYPE_ITEM +
                "children: narrower(type: \"occupation-name\") {" + this.GRAPHQL_DEFAULT_ITEM + "}"
            )) + this.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT;

        this.GRAPHQL_ISCO_STRUCTURE = 
			this.graphQlGenerateQuery(this.graphQlConceptQuery(null, "type: \"isco-level-4\"",
				this.GRAPHQL_DEFAULT_ITEM +
				"isco_code: isco_code_08 "
			));

        this.GRAPHQL_OCCUPATIONS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery(null, "type: \"occupation-name\"", 
                this.GRAPHQL_DEFAULT_ITEM
            ));

        this.GRAPHQL_FIELDS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery(null, "type: \"occupation-field\"",
                this.GRAPHQL_DEFAULT_ITEM +
                "children: narrower(type: \"ssyk-level-4\") {" +
                this.GRAPHQL_SSYK_LIST_TYPE_ITEM +
                    "children: narrower(type: \"occupation-name\") {" + this.GRAPHQL_DEFAULT_ITEM + "}" +
                "}"
            )) + this.GRAPHQL_SSYK_LIST_TYPE_FRAGMENT;

        this.GRAPHQL_SKILLS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery(null, "type: \"skill-headline\"",
                this.GRAPHQL_DEFAULT_ITEM +
                "children: narrower(type: \"skill\") {" + 
                    this.GRAPHQL_DEFAULT_ITEM + 
                "}"
            ));

        this.GRAPHQL_SKILL_COLLECTIONS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("skill_collections", "type: \"skill-collection\"",
                this.GRAPHQL_DEFAULT_ITEM +
                "children: related(type: \"skill\") {" + 
                    this.GRAPHQL_DEFAULT_ITEM + 
                "}"
            ));
        
        this.GRAPHQL_GENERIC_SKILLS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("generic_skills", "type: \"skill-group\"",
                this.GRAPHQL_DEFAULT_ITEM +
                "children: narrower(type: \"generic-skill-group\") {" + 
                    this.GRAPHQL_DEFAULT_ITEM + 
                    "children: narrower(type: \"generic-skill\") {" + 
                    this.GRAPHQL_DEFAULT_ITEM +
                "}" +
            "}" ));

        this.GRAPHQL_SWE_SKILLS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery(null, "type: \"swedish-retail-and-wholesale-council-skill\"",
                this.GRAPHQL_DEFAULT_ITEM +
                "related(type: \"occupation-name\") {" + this.GRAPHQL_DEFAULT_ITEM + "}"
            ));

		this.GRAPHQL_INDUSTRY = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("sni", "type: \"sni-level-1\"",
                this.GRAPHQL_DEFAULT_ITEM + 
                "sni_level_code_2007 " +
                "code: sni_level_code_2007 " +
                "children: narrower(type: \"sni-level-2\") {" +
                    this.GRAPHQL_DEFAULT_ITEM + 
                    "sni_level_code_2007 " +
                    "code: sni_level_code_2007 " +
                    "children: narrower(type: \"sni-level-3\") {" +
                        this.GRAPHQL_DEFAULT_ITEM + 
                        "sni_level_code_2007 " +
                        "code: sni_level_code_2007 " +
                        "children: narrower(type: \"sni-level-4\") {" +
                            this.GRAPHQL_DEFAULT_ITEM + 
                            "sni_level_code_2007 " +
                            "code: sni_level_code_2007 " +
                            "children: narrower(type: \"sni-level-5\") {" +
                                this.GRAPHQL_DEFAULT_ITEM + 
                                "sni_level_code_2007 " +
                                "code: sni_level_code_2007 " +
                            "}" +
                        "}" +
                    "}" +
                "}"));

        this.GRAPHQL_KEYWORDS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("keyword", "type: \"keyword\"",
                this.GRAPHQL_DEFAULT_ITEM + 
                "related {" + this.GRAPHQL_DEFAULT_ITEM + "}"
            ));

        this.GRAPHQL_JOB_TITLES = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("job_title", "type: \"job-title\"",
                this.GRAPHQL_DEFAULT_ITEM + 
                "related {" + this.GRAPHQL_DEFAULT_ITEM + "}"
            ));

        this.GRAPHQL_LANGUAGE = 
            this.graphQlGenerateQuery(
                this.graphQlConceptQuery("language", "type: \"language\"",
                    this.GRAPHQL_DEFAULT_ITEM + 
                    "iso_639_3_alpha_2_2007 " +
                    "iso_639_3_alpha_3_2007 "
                ),
                this.graphQlConceptQuery("language_level", "type: \"language-level\"",
                    this.GRAPHQL_DEFAULT_ITEM
                )
            );

        this.GRAPHQL_EDUCATION = 
            this.graphQlGenerateQuery(
                this.graphQlConceptQuery("sun_education_field", "type: \"sun-education-field-1\"",
                    this.GRAPHQL_DEFAULT_ITEM + 
                    "sun_education_field_code_2020 " +
                    "code: sun_education_field_code_2020 " +
                    "children: narrower(type: \"sun-education-field-2\") {" +
                        this.GRAPHQL_DEFAULT_ITEM + 
                        "sun_education_field_code_2020 " +
                        "code: sun_education_field_code_2020 " +
                        "children: narrower(type: \"sun-education-field-3\") {" +
                            this.GRAPHQL_DEFAULT_ITEM + 
                            "sun_education_field_code_2020 " +
                            "code: sun_education_field_code_2020 " +
                            "children: narrower(type: \"sun-education-field-4\") {" +
                                this.GRAPHQL_DEFAULT_ITEM + 
                                "sun_education_field_code_2020 " +
                                "code: sun_education_field_code_2020 " +
                            "}" +
                        "}" +
                    "}"
                ),
                this.graphQlConceptQuery("sun_education_level", "type: \"sun-education-level-1\"",
                    this.GRAPHQL_DEFAULT_ITEM + 
                    "sun_education_level_code_2020 " +
                    "code: sun_education_level_code_2020 " +
                    "children: narrower(type: \"sun-education-level-2\") {" +
                        this.GRAPHQL_DEFAULT_ITEM + 
                        "sun_education_level_code_2020 " +
                        "code: sun_education_level_code_2020 " +
                        "children: narrower(type: \"sun-education-level-3\") {" +
                            this.GRAPHQL_DEFAULT_ITEM + 
                            "sun_education_level_code_2020 " +
                            "code: sun_education_level_code_2020 " +
                        "}" +
                    "}"
                )
            );

        this.GRAPHQL_GEOGRAPHY = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("continent", "type: \"continent\"",
                this.GRAPHQL_DEFAULT_ITEM + 
                "children: narrower(type: \"country\") {" + 
                    "iso_3166_1_alpha_2_2013 " +
                    "iso_3166_1_alpha_3_2013 " +
                    this.GRAPHQL_DEFAULT_ITEM + 
                    "children: narrower(type: \"region\") {" + 
                        this.GRAPHQL_DEFAULT_ITEM + 
                        "nuts_level_3_code_2013 " +
                        "nuts_level_3_code_2021 " +
                        "children: narrower(type: \"municipality\") {" + 
                            this.GRAPHQL_DEFAULT_ITEM + 
                            "lau_2_code_2015 " +
                            "national_nuts_level_3_code_2019 " +
                        "}" +
                    "}" +
                "}"
            ));

        this.GRAPHQL_OCCUPATION_DESC = 
            this.graphQlGenerateQuery(
                this.graphQlConceptQuery("employment_duration", "type: \"employment-duration\"", this.GRAPHQL_DEFAULT_ITEM),
                this.graphQlConceptQuery("work_place_environment", "type: \"work-place-environment\"", this.GRAPHQL_DEFAULT_ITEM),
                this.graphQlConceptQuery("wage_type", "type: \"wage-type\"", this.GRAPHQL_DEFAULT_ITEM),
                this.graphQlConceptQuery("worktime_extent", "type: \"worktime-extent\"", this.GRAPHQL_DEFAULT_ITEM),
                this.graphQlConceptQuery("occupation_experience_year", "type: \"occupation-experience-year\"", this.GRAPHQL_DEFAULT_ITEM),
                this.graphQlConceptQuery("employment_variety", "type: \"employment-variety\"", 
                    this.GRAPHQL_DEFAULT_ITEM + 
                    "employment_type: narrower(type: \"employment-type\") {" + this.GRAPHQL_DEFAULT_ITEM + "}" +
                    "self_employment_type: narrower(type: \"self-employment-type\") {" + this.GRAPHQL_DEFAULT_ITEM + "}"
                ),
            );

        this.GRAPHQL_ESCO_OCCUPATION = 
            this.graphQlGenerateQuery(
                this.graphQlConceptQuery("esco_occupation", "type: \"esco-occupation\"", this.GRAPHQL_DEFAULT_ITEM)
            );
            
        this.GRAPHQL_ESCO_SKILL = 
            this.graphQlGenerateQuery(
                this.graphQlConceptQuery("esco_skill", "type: \"esco-skill\"", this.GRAPHQL_DEFAULT_ITEM)
            );

        this.GRAPHQL_FORECAST_OCCUPATIONS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("forecast_occupation", "type: \"forecast-occupation\"",
                this.GRAPHQL_DEFAULT_ITEM + 
                "children: related(type: \"ssyk-level-4\") {" + 
                    this.GRAPHQL_DEFAULT_ITEM + 
                "}"
            ));

        this.GRAPHQL_BAROMETER_OCCUPATIONS = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("barometer_occupation", "type: \"barometer-occupation\"",
                this.GRAPHQL_DEFAULT_ITEM + 
                "children: related(type: \"occupation-name\") {" + 
                    this.GRAPHQL_DEFAULT_ITEM + 
                "}"
            ));

        this.GRAPHQL_OTHER = 
            this.graphQlGenerateQuery(this.graphQlConceptQuery("driving_licence", "type: \"driving-licence\"", this.GRAPHQL_DEFAULT_ITEM));

        this.GRAPHQL_CONCEPT_BASE =
            "definition " +
            "type " +
            "label: preferred_label " +
            "sort_order " +
            "substitutes(type: \"occupation-name\") {" +
                "id " +
                "type " +
                "label: preferred_label " +
                "percentage: substitutability_percentage " +
            "} " +
            "keywords: related(type: \"keyword\") {" +
                "id " +
                "type " +
                "label: preferred_label " +
            "} " +
            "job_titles: related(type: \"job-title\") {" +
                "id " +
                "type " +
                "label: preferred_label " +
            "} " +
            "isco_codes: broader(type: \"isco-level-4\") {" +
                "code: isco_code_08 " +
                "label: preferred_label " +
            "} " +
            "exact_match {" + this.GRAPHQL_DEFAULT_ITEM + "} " +
            "broad_match {" + this.GRAPHQL_DEFAULT_ITEM + "} " +
            "narrow_match {" + this.GRAPHQL_DEFAULT_ITEM + "} " +
            "close_match {" + this.GRAPHQL_DEFAULT_ITEM + "} ";

        this.GRAPHQL_CONCEPT_SSYK_ITEM =
            "definition " +
            "ssyk_code: ssyk_code_2012 " +
            "field: broader(type: \"occupation-field\") {" +
                "id " +
                "label: preferred_label " +
            "} " +
            "skills: related(type: \"skill\") {" +
                "id " +
                "label: preferred_label " +
                "headline: broader(type: \"skill-headline\") {" +
                    "id " +
                    "label: preferred_label " +
                "} " +
            "} " +
            "isco_codes: related(type: \"isco-level-4\") {" +
                "code: isco_code_08 " +
                "label: preferred_label " +
            "} ";

        this.GRAPHQL_CONCEPT_ISCO_ITEM =
            "definition " +
            "isco_code: isco_code_08 " +
            "ssyk_codes: related(type: \"ssyk-level-4\") {" +
                "code: ssyk_code_2012 " +
                "label: preferred_label " +
            "} ";

        this.GRAPHQL_CONCEPT_SKILL_ITEM =
            "definition " +
            "occupations: related(type: \"ssyk-level-4\") {" +
                "id " +
                "type " +
                "label: preferred_label " +
                "ssyk_code: ssyk_code_2012 " +
            "} " +
            "exact_match {" + this.GRAPHQL_DEFAULT_ITEM + "} " +
            "broad_match {" + this.GRAPHQL_DEFAULT_ITEM + "} " +
            "narrow_match {" + this.GRAPHQL_DEFAULT_ITEM + "} " +
            "close_match {" + this.GRAPHQL_DEFAULT_ITEM + "} " +
            "skill_collections: related(type: \"skill-collection\") {" +
                "id " +
                "type " +
                "label: preferred_label" + 
            "} ";

        this.GRAPHQL_CONCEPT_SSYK_PARENT_ITEM =
            "parent: broader(type: \"ssyk-level-4\") {" +
                "id " +
                "type " +
                "label: preferred_label " +
                "ssyk_code: ssyk_code_2012 " +
            "} ";

        this.lang = this.getArg("lang");
        this.initEncodedImages();
    }

    async initEncodedImages() {
		const toBase64 = (file) => new Promise((resolve, reject) => {
            var img = new Image();
            img.onload = () => {
                var canvas = document.createElement('canvas');
                var ctx = canvas.getContext('2d');
                var dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL("image/png");
                resolve(dataURL)
            };
            img.src = file;
        });
        this.ICON_AF_EXPORT_B64 = await toBase64(this.ICON_AF_EXPORT);
    }
    
    graphQlConceptQuery(name, query, data) {
		var version = this.getArg("v");
		version = version != null ? ", version: \"" + version + "\"" : "";
		var result = (name ? name + ": " : "") + "concepts(" + query + version + ") {";
		if(data) {
			result += data;
		}
		result += "}";
		return result;
	}

	graphQlGenerateQuery() {
		var query = "query Atlas {";
		for(var i=0; i<arguments.length; ++i) {
			query += arguments[i];
		}
		query += "}";
		return query;
	}

    getArg(key) {
		var raw = window.location.hash.split('#');
		if(raw.length == 2) {
			var cmd = raw[1];
			var args = cmd.split('&');
			for(var i=0; i<args.length; ++i) {
				if(args[i].indexOf(key + "=") !== -1) {
					return args[i].split('=')[1]; 
				}
			}
		}
		return null;
    }
    
    replaceArg(key, value) {
		var raw = window.location.hash.split('#');
		if(raw.length == 2) {
			var cmd = raw[1];
            var args = cmd.split('&');
            var found = false;
            // replace value
			for(var i=0; i<args.length; ++i) {
				if(args[i].indexOf(key + "=") !== -1) {
                    if(value == null) {
                        args[i] = null;
                    } else {
                        args[i] = key + "=" + value;
                    }
                    found = true;
                    break;
				}
            }
            if(!found && value != null) {
                args.push(key + "=" + value);
            }
            args = args.filter(Boolean);
            // rebuild string
            var h = "#";
			for(var i=0; i<args.length; ++i) {
                h += args[i];
                if(i < args.length - 1) {
                    h += "&";
                }
            }
            window.location.hash = h;
        } else if(value) {
            window.location.hash = key + "=" + value;
        }
    }
}

export default new Constants;