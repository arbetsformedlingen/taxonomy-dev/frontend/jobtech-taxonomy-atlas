import React from 'react';
import ReactDOM from 'react-dom';
import Support from './../support.jsx';
import Header from './../components/header.jsx';
import Footer from './../components/footer.jsx';
import Loader from './../components/loader.jsx';
import Rest from './../context/rest.jsx';
import Constants from './../context/constants.jsx';
import Localization from './../context/localization.jsx';
import WorkForm from './feedback_form/work_form.jsx';
import SkillForm from './feedback_form/skill_form.jsx';
import OtherForm from './feedback_form/other_form.jsx';

class FeedbackForm extends React.Component { 

	constructor() {
        super();
        Support.init();
        this.state = {
            type: Constants.FEEDBACK_TYPE_WORK,
        };
    }

    onTypeSelected(e) {
        this.setState({type: e.target.value});
    }

    renderTitle() {
        return (
            <h1 className="title font">
                {Localization.get("taxonomy_feedback_content")}
            </h1>
        );
    }

    renderTypeSelector() {
        return (
            <select
                className="type_selector rounded"
                value={this.state.type}
                onChange={this.onTypeSelected.bind(this)}>
                <option value={Constants.FEEDBACK_TYPE_WORK}>
                    {Localization.get("feedback_occupation")}
                </option>
                <option value={Constants.FEEDBACK_TYPE_SKILL}>
                    {Localization.get("feedback_skill")}
                </option>
                <option value={Constants.FEEDBACK_TYPE_OTHER}>
                    {Localization.get("feedback_something_else")}
                </option>
            </select>
        );
    }

    renderForm() {
        if(this.state.type == Constants.FEEDBACK_TYPE_WORK) {
            return ( <WorkForm/> );
        } else if(this.state.type == Constants.FEEDBACK_TYPE_SKILL) {
            return ( <SkillForm/> );
        } else if(this.state.type == Constants.FEEDBACK_TYPE_OTHER) {
            return ( <OtherForm/> );
        }
    }

    render() {
        return (
            <div className="feedback_form_page">
                <div className="page_content">
                    <div className="form_body">
                        {this.renderTitle()}
                        {this.renderTypeSelector()}
                        {this.renderForm()}
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
	
}

ReactDOM.render(<FeedbackForm/>, document.getElementById('content'));