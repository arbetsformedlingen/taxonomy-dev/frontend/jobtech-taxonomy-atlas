import React from 'react';
import ReactDOM from 'react-dom';
import Support from './../support.jsx';
import Loader from './../components/loader.jsx';
import AutocompleteSearch from './../components/autocomplete_search.jsx';
import Rest from './../context/rest.jsx';
import Util from './../context/util.jsx';
import Constants from './../context/constants.jsx';
import Localization from './../context/localization.jsx';

class Example extends React.Component { 

	constructor() {
        super();
		Support.init();
        this.state = {
            showingInfo: true,
            item: null,
            queryInfo: null,
            page: 0,
            items: [],
		};
	}

    onAutocompleteSearch(query, version, onSuccess, onError) {
        var ql =
            "  concepts(type: \"keyword\", preferred_label_contains: \"" + query + "\") {\n" +
            "    preferred_label\n" +
            "    related(type: \"sun-education-field-4\") {\n" +
            "      id\n" +
            "      preferred_label\n" +
            "      sun_education_field_code_2020\n" +
            "      broader {\n" +
            "        possible_combinations {\n" +
            "          id\n" +
            "          preferred_label\n" +
            "        }\n" +
            "        unlikely_combinations {\n" +
            "          id\n" +
            "          preferred_label\n" +
            "        } \n" +
            "      }\n" +
            "    }\n" +
            "  }";
        
        Rest.getGraphQL(ql, (data) => {
            var items = data.data.concepts.filter((x) => {
                return x.related.length > 0;
            });
            for(var i=0; i<items.length; ++i) {
                items[i].preferredLabel = items[i].preferred_label;
                console.log(items[i]);
            }
            this.setState({
                items: Util.sortByKey(items, "preferred_label", true)
            });
            onSuccess(items);
        }, onError);
    }

    onAutocompleteSuggestions(items) {
        // prep
        for(var i=0; i<items.length; ++i) {
            var item = items[i];
            for(var j=0; j<item.related.length; ++j) {
                item.related[j].keywords = [];
            }
        }
        // setup
        var result = [];
        for(var i=0; i<items.length; ++i) {
            var item = items[i];
            for(var j=0; j<item.related.length; ++j) {
                var related = item.related[j];
                var r = result.find((x) => { return x.id == related.id; });
                if(r == null) {
                    result.push(related);
                }
                related.keywords.push(item);
            }
        }
        return result;
    }

    onAutocompleteSelected(item, queryInfo) {
        this.setState({
            item: item,
            queryInfo: queryInfo,
        });
    }

    onRenderItem(item) {
        var keywords = item.keywords.map((keyword, index) => {
            return (
                <span key={index}>
                    {keyword.preferred_label}{index == item.keywords.length - 1 ? "" : " ,"}
                </span>
            );
        });
        return (
            <span>
                {item.preferred_label}<br/>
                <span className="autocomplete_keyword">
                    keyword({item.keywords.length}): {keywords}
                </span>
            </span>
        );
    }

    onToggleClicked() {
        this.setState({showingInfo: !this.state.showingInfo});
    }

    onPageClicked(page) {
        this.setState({page: page});
    }

    getValue(name) {
        if(this.state.item) {
            return this.state.item[name];
        }
        return "";
    }

    renderTitle() {
        return (
            <div 
                alt={Localization.get("alt_homepage")}
                className="header_title">
                <div>
                    <div>JobTech</div>
                    <div>Atlas</div>
                </div>
                <div>{Localization.get("sub_title")}</div>
            </div>
        );
    }

    renderNumber(number) {
        if(this.state.showingInfo) {
            return (
                <div className="example_number">{number}</div>
            );
        }
    }

    renderCombinationDropDown(data) {
        var items = Util.sortByKey(data, "preferred_label", true).map((item, key) => {
            return ( <option key={key}>{item.preferred_label}</option> );
        });
        return ( <select>{items}</select> );
    }

    renderItem() {
        var keywords = this.state.item.keywords.map((keyword, index) => {
            return (
                <span key={index}>
                    {keyword.preferred_label}{index == this.state.item.keywords.length - 1 ? "" : " ,"}
                </span>
            );
        });
        return (
            <div className="concept_info example_number_container">
                <div className="concept_info_sub_title">Valt begrepp från sökning</div>
                <div>{this.getValue("preferred_label")}</div>
                <div className="concept_info_sub_title">SUN-nivå</div>
                <div>{this.getValue("sun_education_field_code_2020")}</div>
                <div className="concept_info_combinations">
                    <div>
                        <div className="concept_info_sub_title">Sannolika kombinationer</div>
                        {this.renderCombinationDropDown(this.state.item.broader[0].possible_combinations)}
                    </div>
                    <div>
                        <div className="concept_info_sub_title">Osannolika kombinationer</div>
                        {this.renderCombinationDropDown(this.state.item.broader[0].unlikely_combinations)}
                    </div>
                </div>
                <div className="concept_info_sub_title">Keywords med relation till detta koncept</div>
                <div>{keywords}</div>
            </div>
        );
    }

    renderDemo() {
        return (
            <div className="example_content">
                <div className="example_number_container">
                    <AutocompleteSearch
                        highlightQuery={false}
                        showType={false}
                        customSearch={this.onAutocompleteSearch.bind(this)}
                        customSuggestions={this.onAutocompleteSuggestions.bind(this)}
                        onRenderItem={this.onRenderItem.bind(this)}
                        placeholder="Sök efter utbildningsinriktning"
                        callback={this.onAutocompleteSelected.bind(this)}/>
                </div>
                {this.state.item != null &&
                    this.renderItem()
                }
            </div>
        );
    }

    renderGraphQLQuery() {
        return (
            <div className={"info_api graphql_query api_page_" + this.state.page}>
                <div>{"concepts(type: \"keyword\", preferred_label_contains: \"query\") {"}</div>
                <div>{"\trelated(type: \"sun-education-field-4\") {"}</div>
                <div>{"\t\tid"}</div>
                <div>{"\t\tpreferred_label"}</div>
                <div>{"\t\tsun_education_field_code_2020"}</div>
                <div>{"\t\tbroader {"}</div>
                <div>{"\t\t\tpossible_combinations {"}</div>
                <div>{"\t\t\t\tid"}</div>
                <div>{"\t\t\t\tpreferred_label"}</div>
                <div>{"\t\t\t}"}</div>
                <div>{"\t\t\tunlikely_combinations {"}</div>
                <div>{"\t\t\t\tid"}</div>
                <div>{"\t\t\t\tpreferred_label"}</div>
                <div>{"\t\t\t}"}</div>
                <div>{"\t\t}"}</div>
                <div>{"\t}"}</div>
                <div>{"}"}</div>
            </div>
        );
    }
    
    renderButton(page, inactive) {
        var css = inactive == null ? (this.state.page == page ? "button active" : "button") : "button inactive";
        return (
            <div
                className={css}
                onPointerUp={this.onPageClicked.bind(this, page)}>
                {page + 1}
            </div>
        );
    }

    renderText(page) {
        if(page == 0) {
            return (
                <div>
                    <span>För denna demonstration används ett GraphQL-anropp.</span><br/>
                    <span>Så här ser den kompletta GraphQL-queryn ut.</span>
                </div>
            );
        } else if(page == 1) {
            return (
                <div>
                    <span>Anroppet börjar med att söka efter alla koncept av typen <b>keyword</b> som innehåller användarens sökterm.</span><br/>
                    <span>Resultatet är en lista av alla de koncept sökningen fick träff på.</span>
                </div>
            );
        } else if(page == 2) {
            return (
                <div>
                    <span>Klienten filtrerar resultatet och behåller endast de koncept som har <b>related</b> relationskopplingar.</span><br/>
                    <span>Om ett koncept inte har några relationskopplingar är listan tom.</span>
                </div>
            );
        } else if(page == 3) {
            return (
                <div>
                    <span>Klienten visar alla unika <b>sun-education-field-4</b> koncept med dess <b>preferred_label</b> som förslag för användaren.</span><br/>
                    <span>Klienten filtrerar alla <b>sun-education-field-4</b> genom dess <b>id</b>, så att det inte förkommer några dubbleter.</span><br/>
                    <span>Detta är viktigt då flera <b>keyword</b> kan vara kopplade till samma koncept.</span>
                </div>
            );
        } else if(page == 4) {
            return (
                <div>
                    <span>När användaren valt ett sökförslag presenteras all information om det valda konceptet.</span>
                </div>
            );
        }
    }

    renderWalkthroughPage() {
        var renderButton = (page) => {
            return (
                <div
                    className={this.state.page == page ? "button active" : "button"}
                    onPointerUp={this.onPageClicked.bind(this, page)}>
                    {page + 1}
                </div>
            );
        };
        return (
            <div className="info_page">
                <div className="page_description">
                    <div className="description">
                        {this.renderText(this.state.page)}
                        <div className="buttons">
                            {this.renderButton(0)}
                            {this.renderButton(1)}
                            {this.renderButton(2)}
                            {this.renderButton(3)}
                            {this.renderButton(4)}
                        </div>
                    </div>
                    {this.renderGraphQLQuery()}
                </div>
            </div>
        );
    }

    renderWalkthrough() {
        return (
            <div className="example_content info_group"> 
                <div
                    className="expand_button" 
                    onPointerUp={this.onToggleClicked.bind(this)}>
                    {this.state.showingInfo ? "Dölj" : "Visa"}
                </div>
                {this.state.showingInfo &&
                    <div className="info_content">
                        {this.renderWalkthroughPage()}
                    </div>
                }
            </div>
        );
    }

    renderDescription() {
        return (
            <div className="demo_description">
                <span>Syftet med denna demo är att visa hur en klient kan få en kombination av SUN-nivå (nivå 2) och SUN-inriktning (nivå 4) med hjälp av ett GraphQL-anrop.
Demon använder sig av de sökbegrepp (keyword) som finns kopplade till SUN-inriktningar för att hjälpa användaren att hitta rätt utbildningsinriktning. Utöver det används relationstyperna "possible-combination" och "unlikely-combination" för att, utifrån den utbildningsinriktning som valts, avgränsa vilka utbildningsnivåer som är möjliga för den inriktningen.</span><br/><br/>
                <span>Ett vanligt användningsfall är att behöva registrera sin högsta utbildningsnivå kombinerat med utbildningens inriktning. En svårighet för användaren är att utbildningsinriktningarnas namn inte alltid känns beskrivande för den utbildning som användaren själv gått. En annan svårighet är att vissa utbildningsinriktningar inte ska kunna kombineras med vissa utbildningsnivåer.</span><br/><br/>
                <span>För att lösa de här problemen har SCB tagit fram vanliga sökbegrepp/populärbenämningar för utbildningsinriktningarna och ett kopplingsschema med giltiga kombinationer mellan utbildningsnivå och utbildningsinriktning. Demon använder sig av dessa för att göra navigering i SUN-strukturen enklare.</span><br/><br/>
                <span>Flödet börjar med att val av utbildningsinriktning görs, därefter utbildningsnivå.</span>
            </div>
        );
    }

    render() {
        return (
            <div className="page font">
                {this.renderTitle()}
                {this.renderDescription()}
                <div className="text_header">API Demo: Hitta SUN-inriktning och SUN-nivå med hjälp av sökbegrepp</div>
				<div className="example_group">
                    {this.renderDemo()}
                </div>
                <div className="text_header">Genomgång</div>
                <div className="example_group">
                    {this.renderWalkthrough()}
                </div>
            </div>
        );
    }
	
}

ReactDOM.render(<Example/>, document.getElementById('content'));