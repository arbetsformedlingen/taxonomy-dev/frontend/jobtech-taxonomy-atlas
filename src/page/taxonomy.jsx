import React from 'react';
import ReactDOM from 'react-dom';
import Support from './../support.jsx';
import Header from './../components/header.jsx';
import Footer from './../components/footer.jsx';
import Loader from './../components/loader.jsx';
import TreeView from './../components/treeview.jsx';
import Constants from './../context/constants.jsx';
import Search from './taxonomy/search.jsx';
import Content from './taxonomy/content.jsx';
import Start from './taxonomy/start.jsx';
import GotoDialog from './taxonomy/goto_dialog.jsx';
import Util from './taxonomy/util.jsx';
import EventDispatcher from './../context/event_dispatcher.jsx';
import Localization from './../context/localization.jsx';
import Rest from './../context/rest.jsx';
import Context from './../context/context.jsx';
import { isObject } from 'vis-util';

class Taxonomy extends React.Component { 

	constructor() {
        super();
		Support.init();
		// constants
		this.SHOW_STRUCTURE = 0;
		this.SHOW_GROUPS = 1;
		this.SHOW_OCCUPATIONS = 2;
		this.SHOW_FIELDS = 3;
		this.SHOW_ISCO = 4;
		this.SHOW_SKILLS = 5;
		this.SHOW_SKILL_COLLECTIONS = 6;
		this.SHOW_GENERIC_SKILLS = 7;
		this.SHOW_OTHER = 8;
        this.SHOW_WORK_DESC = 9;
        this.SHOW_GEOGRAPHY = 10;
        this.SHOW_INDUSTRY = 11;
        this.SHOW_SEARCH = 12;
        this.SHOW_LANGUAGE = 13;
        this.SHOW_EDUCATION = 14;
        this.SHOW_ESCO_OCCUPATION = 15;
        this.SHOW_ESCO_SKILL = 16;
        this.SHOW_FORECAST = 17;
        this.SHOW_BAROMETER_OCCUPATION = 18;
        this.SHOW_JOB_TITLE = 19;
        this.SHOW_SWE_SKILLS = 20;
		// state
        this.state = {
			items: [],
			selected: null,
			filter: "",
			isLoading: true,
			isFetchingItem: false,
			dialog: null,
			popupText: null,
			popupSpinner: true,
			showType: -1,
		};
		this.preSelectType = null;
		Context.escoLoadCompleteCallback = (items, isOccupations) => {
			if((this.fetchType == this.SHOW_ESCO_OCCUPATION && isOccupations) || 
			   (this.fetchType == this.SHOW_ESCO_SKILL && !isOccupations)) {
				this.setState({
					showType: this.fetchType,
					items: items,
					isLoading: false,
				});
			}
		};
		Context.skillLoadCompleteCallback = (items) => {
			if(this.fetchType == this.SHOW_SKILLS) {
				this.setState({
					showType: this.SHOW_SKILLS,
					items: items,
					isLoading: false,
				});
			}
		};
		setTimeout(() => { Context.init(); }, 2500);
	}
	
	componentDidMount() {
        EventDispatcher.add(this.forceUpdate.bind(this), Constants.EVENT_LANGUAGE_CHANGED);
        EventDispatcher.add(this.onShowPopup.bind(this), Constants.EVENT_SHOW_POPUP_INDICATOR);
        EventDispatcher.add(this.onHidePopup.bind(this), Constants.EVENT_HIDE_POPUP_INDICATOR);
        EventDispatcher.add(this.onAutocompleteItemClicked.bind(this), Constants.EVENT_AUTOCOMPLETE_ITEM);
		// check if url points to a targeted concept
		var hashConcept = Constants.getArg("concept");
		if(hashConcept && hashConcept.length > 3) {
			var version = Constants.getArg("v");
			Rest.getConcept(hashConcept, version, (data) => {
				if(data.length > 0) {
					var isField = data[0].type == "occupation-field";
					var type = Util.getDisplayTypeForConcept(data[0].type);
					this.onDisplayTypeChanged(type, {
						key: "id",
						value: data[0].id,
					}, data[0].type);
					EventDispatcher.fire(Constants.EVENT_TAXONOMY_SET_SEARCH_TYPE, {
						type: type,
						radio: isField ? Constants.DISPLAY_WORK_TYPE_FIELDS : Constants.DISPLAY_WORK_TYPE_GROUP
					});
				}
			});
		} else {
			this.showGroups();
		}
	}

	setPrefetchedTreeViewData(type, isLoading, items) {
		if(this.state.showType != type) {
			this.fetchType = type;
			if(isLoading) {
				this.setState({isLoading: true});
			} else {
				this.setState({
					showType: type,
					items: items,
					isLoading: false,
				});
			}
		}
	}

	async setTreeViewData(type, method) {
		if(this.state.showType != type) {
			this.fetchType = type;
			this.setState({isLoading: true});
			var data = await method();
			if(this.fetchType == type) {
				this.setState({
					showType: type,
					items: data,
					isLoading: false,
				});
			}
		}
	}

	async showStructure() {
		await this.setTreeViewData(this.SHOW_STRUCTURE, () => { return Util.getSsykStructure(); });
	}

	async showGroups() {
		await this.setTreeViewData(this.SHOW_GROUPS, () => { return Util.getSsykGroups(); });
	}

	async showOccupations() {
		await this.setTreeViewData(this.SHOW_OCCUPATIONS, () => { return Util.getOccupations(); });
	}

	async showFields() {
		await this.setTreeViewData(this.SHOW_FIELDS, () => { return Util.getFields(); });
	}

	async showIsco() {
		await this.setTreeViewData(this.SHOW_ISCO, () => { return Util.getIscoStructure(); });
	}

	async showSkills() {
		//await this.setTreeViewData(this.SHOW_SKILLS, () => { return Util.getSkills(); });
		this.setPrefetchedTreeViewData(this.SHOW_SKILLS, Context.isLoadingSkills, Context.skillItems);
	}
	
	async showSkillCollections() {
		await this.setTreeViewData(this.SHOW_SKILL_COLLECTIONS, () => {return Util.getSkillCollections(); })
	}

	async showGenericSkills() {
		await this.setTreeViewData(this.SHOW_SKILL_COLLECTIONS, () => {return Util.getGenericSkills(); })
	}
	async showSweSkills() {
		await this.setTreeViewData(this.SHOW_SWE_SKILLS, () => { return Util.getSweSkills(); });
	}

	async showOccupationDesc() {
		await this.setTreeViewData(this.SHOW_WORK_DESC, () => { return Util.getOccupationDesc(); });
	}

	async showGeography() {
		await this.setTreeViewData(this.SHOW_GEOGRAPHY, () => { return Util.getGeography(); });
	}

	async showIndustry() {
		await this.setTreeViewData(this.SHOW_INDUSTRY, () => { return Util.getIndustry(); });
	}

	async showKeyword() {
		await this.setTreeViewData(this.SHOW_SEARCH, () => { return Util.getKeywords(); });
	}

	async showJobTitles() {
		await this.setTreeViewData(this.SHOW_JOB_TITLE, () => { return Util.getJobTitles(); });
	}

	async showLanguage() {
		await this.setTreeViewData(this.SHOW_LANGUAGE, () => { return Util.getLanguage(); });
	}

	async showEducation() {
		await this.setTreeViewData(this.SHOW_EDUCATION, () => { return Util.getEducation(); });
	}

	async showOther() {
		await this.setTreeViewData(this.SHOW_OTHER, () => { return Util.getOthers(); });
	}

	async showEscoOccupation() {
		//await this.setTreeViewData(this.SHOW_ESCO_OCCUPATION, () => { return Util.getEscoOccupation(); });
		this.setPrefetchedTreeViewData(this.SHOW_ESCO_OCCUPATION, Context.isLoadingEscoOccupation, Context.escoOccuptionItems);
	}
	async showEscoSkill() {
		//await this.setTreeViewData(this.SHOW_ESCO_SKILL, () => { return Util.getEscoSkill(); });
		this.setPrefetchedTreeViewData(this.SHOW_ESCO_SKILL, Context.isLoadingEscoSkills, Context.escoSkillItems);
	}

	async showForecast() {
		await this.setTreeViewData(this.SHOW_FORECAST, () => { return Util.getForecast(); });
	}

	async showBarometerOccupation() {
		await this.setTreeViewData(this.SHOW_BAROMETER_OCCUPATION, () => { return Util.getBarometerOccupations(); });
	}

	async fetchItem(item) {
		this.setState({isFetchingItem: true});
		await Util.setupItem(item);
		this.setState({
			selected: item,
			isFetchingItem: false,
		}, () => {
			Constants.replaceArg("concept", item.id);
		});
	}

	onDisplayTypeChanged(type, preSelectType, conceptType) {
		this.preSelectType = preSelectType;
		if(type == Constants.DISPLAY_TYPE_WORK_SSYK) {
			if(conceptType != null && conceptType == "occupation-field") {
				this.showFields();
			} else {
				this.showGroups();
			}
		} else if(type == Constants.DISPLAY_TYPE_WORK_ISCO) {
			this.showIsco();
		} else if(type == Constants.DISPLAY_TYPE_SKILL) {
			this.showSkills();
		} else if(type == Constants.DISPLAY_TYPE_SKILL_COLLECTION) {
			this.showSkillCollections();
		} else if(type == Constants.DISPLAY_TYPE_SWE_SKILL) {
			this.showSweSkills();
		} else if(type == Constants.DISPLAY_TYPE_WORK_DESC) {
			this.showOccupationDesc();
		} else if(type == Constants.DISPLAY_TYPE_GEOGRAPHY) {
			this.showGeography();
		} else if(type == Constants.DISPLAY_TYPE_INDUSTRY) {
			this.showIndustry();
		} else if(type == Constants.DISPLAY_TYPE_SEARCH) {
			this.showKeyword();
		} else if(type == Constants.DISPLAY_TYPE_LANGUAGE) {
			this.showLanguage();
		} else if(type == Constants.DISPLAY_TYPE_EDUCATION) {
			this.showEducation();
		} else if(type == Constants.DISPLAY_TYPE_ESCO_OCCUPATION) {
			this.showEscoOccupation();
		} else if(type == Constants.DISPLAY_TYPE_ESCO_SKILL) {
			this.showEscoSkill();
		} else if(type == Constants.DISPLAY_TYPE_FORECAST_OCCUPATION) {
			this.showForecast();
		} else if(type == Constants.DISPLAY_TYPE_BAROMETER_OCCUPATION) {
			this.showBarometerOccupation();
		} else if(type == Constants.DISPLAY_TYPE_OTHER) {
			this.showOther();
		} else if(type == Constants.DISPLAY_TYPE_JOB_TITLE) {
			this.showJobTitles();
		} else if(type == Constants.DISPLAY_TYPE_GENERIC_SKILL) {
			this.showGenericSkills();
		} 
	}

	onDisplayWorkTypeChanged(type) {
		this.preSelectType = null;
		if(type == Constants.DISPLAY_WORK_TYPE_STRUCTURE) {
			this.showStructure();
		} else if(type == Constants.DISPLAY_WORK_TYPE_GROUP) {
			this.showGroups();
		} else if(type == Constants.DISPLAY_WORK_TYPE_OCCUPATIONS) {
			this.showOccupations();
		} else if(type == Constants.DISPLAY_WORK_TYPE_FIELDS) {
			this.showFields();
		}
		this.setState({filter: ""});
	}

	onFilterChanged(filter) {
		this.preSelectType = null;
		this.setState({filter: filter});
	}

	onItemClicked(item) {
		this.preSelectType = null;
		if(item != this.state.selected && item.type) {
			this.fetchItem(item);
			/*if(item.children != null && item.children.length > 0) {
				setTimeout(() => {
					for(var i=0; i<item.children.length && i<20; ++i) {
						Util.setupItem(item.children[i]);
					}
				}, 250);
			}*/
		}
		if(item.isExpanded != null) {
			item.isExpanded = !item.isExpanded;
		}
	}

	onAutocompleteItemClicked(item) {
		// TODO: populate special fields
		item.label = item.preferredLabel;
		this.onOtherPressed(item, true);
	}

	showGotoDialog(text, callback, skipDialog) {
		var yesCallback = () => {
			callback();
			this.setState({
				dialog: null,
				filter: "",
			});
		};
		if(skipDialog) {
			yesCallback();
		} else {
			var dialog = 
				<GotoDialog 
					text={text}
					yesCallback={yesCallback}
					noCallback={() => this.setState({dialog: null})}/>;
			this.setState({dialog: dialog});
		}
	}
	
	// the following 3 callbacks are called from the special tags in the info block
	onSsykPressed(ssyk) {
		this.showGotoDialog(Localization.get("db_ssyk-level-4") + ", " + ssyk.label, () => {
			this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, Constants.DISPLAY_WORK_TYPE_GROUP, "ssyk_code", ssyk.code, this.showStructure.bind(this));
		});
	}
	
	onIscoPressed(isco) {
		this.showGotoDialog(Localization.get("db_isco-level-4") + ", " + isco.label, () => {
			this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_ISCO, 1, "isco_code", isco.code, this.showIsco.bind(this));
		});
	}
	
	onFieldPressed(field) {
		this.showGotoDialog(Localization.get("db_occupation-field") + ", " + field.label, () => {
			this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, Constants.DISPLAY_WORK_TYPE_GROUP, "id", field.id, this.showFields.bind(this));
		});
	}

	onOtherPressed(item, skipDialog) {
		if(item.type == "skill" || item.type == "skill-headline") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_SKILL, 1, "id", item.id, this.showSkills.bind(this));
			}, skipDialog);

		} else if(item.type == "skill-collection") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_SKILL_COLLECTION, 1, "id", item.id, this.showSkillCollections.bind(this));
			}, skipDialog);	
		} else if(item.type == "skill-group") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_SKILL_COLLECTION, 1, "id", item.id, this.showGenericSkills.bind(this));
			}, skipDialog);	
		} else if(item.type == "ssyk-level-4") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, 1, "id", item.id, this.showGroups.bind(this));
			}, skipDialog);
		} else if(item.type.startsWith("ssyk-level-")) {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, Constants.DISPLAY_WORK_TYPE_STRUCTURE, "id", item.id, this.showStructure.bind(this));
			}, skipDialog);
		} else if(item.type == "isco-level-4") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_ISCO, 1, "id", item.id, this.showIsco.bind(this));
			}, skipDialog);
		} else if(item.type.startsWith("occupation-")) {
			var isField = item.type != "occupation-name";
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_SSYK, isField ? Constants.DISPLAY_WORK_TYPE_FIELDS : Constants.DISPLAY_WORK_TYPE_GROUP, "id", item.id, isField ? this.showFields.bind(this) : this.showGroups.bind(this));
			}, skipDialog);
		} else if(item.type == "keyword") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_SEARCH, 1, "id", item.id, this.showKeyword.bind(this));
			}, skipDialog);
		} else if(item.type == "job-title") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_JOB_TITLE, 1, "id", item.id, this.showJobTitles.bind(this));
			}, skipDialog);
		} else if(item.type == "employment_duration" || 
		 		  item.type == "employment_type" ||
		 		  item.type == "wage_type" ||
		 		  item.type == "worktime_extent" ||
		 		  item.type == "occupation_experience_year") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_WORK_DESC, 1, "id", item.id, this.showOccupationDesc.bind(this));
			}, skipDialog);
		} else if(item.type.startsWith("sni-")) {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_INDUSTRY, 1, "id", item.id, this.showIndustry.bind(this));
			}, skipDialog);
		} else if(item.type == "language" || 
				  item.type == "language-level") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_LANGUAGE, 1, "id", item.id, this.showLanguage.bind(this));
			}, skipDialog);
		} else if(item.type == "continent" || 
				  item.type == "country" || 
				  item.type == "region" || 
				  item.type == "municipality") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_GEOGRAPHY, 1, "id", item.id, this.showGeography.bind(this));
			}, skipDialog);
		} else if(item.type.startsWith("sun-education-")) {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_EDUCATION, 1, "id", item.id, this.showEducation.bind(this));
			}, skipDialog);
		} else if(item.type == "swedish-retail-and-wholesale-council-skill") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_SWE_SKILL, 1, "id", item.id, this.showSweSkills.bind(this));
			}, skipDialog);
		} else if(item.type == "esco-occupation") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_ESCO_OCCUPATION, 1, "id", item.id, this.showEscoOccupation.bind(this));
			}, skipDialog);
		} else if(item.type == "esco-skill") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_ESCO_SKILL, 1, "id", item.id, this.showEscoSkill.bind(this));
			}, skipDialog);
		} else if(item.type == "forecast-occupation") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_FORECAST_OCCUPATION, 1, "id", item.id, this.showForecast.bind(this));
			}, skipDialog);
		} else if(item.type == "barometer-occupation") {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_BAROMETER_OCCUPATION, 1, "id", item.id, this.showBarometerOccupation.bind(this));
			}, skipDialog);
		} else {
			this.showGotoDialog(Localization.get("db_" + item.type) + ", " + item.label, () => {
				this.switchAndSelect(Constants.DISPLAY_TYPE_OTHER, 1, "id", item.id, this.showOther.bind(this));
			}, skipDialog);
		}
	}

    onShowPopup(data) {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.add("save_enter_margin");
		if(data instanceof Object) {
			this.setState({
				popupText: data.text,
				popupSpinner: data.showSpinner,
			});
		} else {
			this.setState({
				popupText: data,
				popupSpinner: true,
			});
		}
    }

    onHidePopup() {
        var indicator = document.getElementById("save_indicator");
        indicator.classList.remove("save_enter_margin");
    }

	switchAndSelect(type, radio, key, value, showMethod) {
		EventDispatcher.fire(Constants.EVENT_TAXONOMY_SET_SEARCH_TYPE, {
			type: type,
			radio: radio, //isFields ? Constants.DISPLAY_WORK_TYPE_FIELDS : Constants.DISPLAY_WORK_TYPE_GROUP
		});
		this.preSelectType = {
			key: key,
			value: value,
		};
		this.setState({isFetchingItem: true});
		showMethod();
	}

	onPreSelect(item) {
		if(this.preSelectType) {
			if(this.preSelectType.key == "ssyk_code") {
				return item.ssyk_code && item.ssyk_code.code == this.preSelectType.value;
			} else if(this.preSelectType.key == "isco_code") {
				return item.isco_code && item.isco_code.code == this.preSelectType.value;
			}
			return item[this.preSelectType.key] == this.preSelectType.value;
		}
		return false;
	}

	renderTreeView() {
		return ( 
			<div className="search_panel">
				<Search 
					onDisplayTypeChanged={this.onDisplayTypeChanged.bind(this)}
					onDisplayWorkTypeChanged={this.onDisplayWorkTypeChanged.bind(this)}
					onFilterChanged={this.onFilterChanged.bind(this)}
					filter={this.state.filter}/>
				<TreeView 
					preSelectCallback={this.onPreSelect.bind(this)}
					isLoading={this.state.isLoading}
					filter={this.state.filter}
					roots={this.state.items}
					preSelectObject={this.preSelectType}
					onClick={this.onItemClicked.bind(this)}/> 
			</div>
		);
	}

	renderDialog() {
		if(this.state.dialog) {
			return this.state.dialog;
		}
	}
	
    renderSaveIndicator() {
        return (
            <div className="save_indicator_content">
                <div
                    id="save_indicator" 
                    className="save_indicator font">
					{this.state.popupSpinner ??
                    	<div className="loader"/>	
					}
                    <div>
                        {this.state.popupText}
                    </div>
                </div>
            </div>
        );
	}
	
	renderContent() {
		if(this.state.selected == null) {
			return (
				<Start/>
			);
		} else {
			return (
				<Content 
					ssykCallback={this.onSsykPressed.bind(this)}
					iscoCallback={this.onIscoPressed.bind(this)}
					fieldCallback={this.onFieldPressed.bind(this)}
					otherCallback={this.onOtherPressed.bind(this)}
					isFetchingItem={this.state.isFetchingItem}
					item={this.state.selected}/>
			);
		}
	}

    render() {
        return (
            <div className="taxonomy_page">
                <Header/>
				<div className="page_content">
					{this.renderTreeView()}
					{this.renderContent()}
					{this.renderDialog()}
				</div>
				{this.renderSaveIndicator()}
                <Footer/>
            </div>
        );
    }
	
}

ReactDOM.render(<Taxonomy/>, document.getElementById('content'));