import React from 'react';
import ReactDOM from 'react-dom';
import Support from '../support.jsx';
import Header from '../components/header.jsx';
import Footer from '../components/footer.jsx';
import Rest from '../context/rest.jsx';
import Constants from '../context/constants.jsx';
import Localization from '../context/localization.jsx';
import TaxonomyUtil from './taxonomy/util.jsx';
import TreeView from '../components/treeview.jsx';
import EventDispatcher from '../context/event_dispatcher.jsx';

class About extends React.Component { 

	constructor() {
        super();
        Support.init();
        this.state = {
        };
    }

    componentDidMount() {
        EventDispatcher.add(this.forceUpdate.bind(this), Constants.EVENT_LANGUAGE_CHANGED);
    }

    render() {
        return (
            <div className="about_page">
                <Header/>
                <div className="page_container">
                    <div className="page_content font">
                        <div className="margin">
                            {Localization.get("about_content")}
                        </div>
                        <img
                            className="graph_image"
                            src="./../resources/graph.svg"/>
                        <a 
                            href="./../resources/graph.svg" download
                            className="graph_download_link">
                            {Localization.get("download_image")}
                        </a>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
	
}

ReactDOM.render(<About/>, document.getElementById('content'));