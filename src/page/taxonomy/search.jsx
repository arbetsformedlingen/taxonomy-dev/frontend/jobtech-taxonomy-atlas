import React from 'react';
import ReactDOM from 'react-dom';
import ContextUtil from './../../context/util.jsx';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';

class Search extends React.Component { 

	constructor() {
        super();
		// state
        this.state = {
			isShowingRadio: true,
			filter: "",
			displayType: Constants.DISPLAY_TYPE_WORK_SSYK,
			displayRadioValue: Constants.DISPLAY_WORK_TYPE_GROUP,
        };
	}

	componentDidMount() {
		EventDispatcher.add(this.onSetSearchType.bind(this), Constants.EVENT_TAXONOMY_SET_SEARCH_TYPE);
	}

	componentWillReceiveProps(props) {
		this.setState({filter: props.filter});
	}

	onSetSearchType(value) {
		this.setState({
			isShowingRadio: value.type == Constants.DISPLAY_TYPE_WORK_SSYK,
			displayType: value.type,
			displayRadioValue: value.radio,
		});
	}

	onFilterChanged(e) {
		if(this.props.onFilterChanged) {
			this.props.onFilterChanged(e.target.value);
			this.forceUpdate();
		}
	}

	onDisplayTypeChanged(e) {
		if(this.props.onDisplayTypeChanged) {
			this.props.onDisplayTypeChanged(e.target.value);
		}
		this.setState({
			isShowingRadio: e.target.value == Constants.DISPLAY_TYPE_WORK_SSYK,
			displayType: e.target.value,
			displayRadioValue: Constants.DISPLAY_WORK_TYPE_GROUP,
		});
	}

	onDisplayRadioChanged(e) {
		if(this.props.onDisplayWorkTypeChanged) {
			this.props.onDisplayWorkTypeChanged(e.target.value);
		}
		this.setState({displayRadioValue: e.target.value});
	}

	renderSearchType() {
		var items = [
			{value: Constants.DISPLAY_TYPE_WORK_SSYK, label: Localization.get("occupations_ssyk")},
			{value: Constants.DISPLAY_TYPE_WORK_ISCO, label: Localization.get("occupations_isco")},
			{value: Constants.DISPLAY_TYPE_SKILL, label: Localization.get("skills")},
			{value: Constants.DISPLAY_TYPE_GENERIC_SKILL, label: Localization.get("generic_skills")},
			{value: Constants.DISPLAY_TYPE_SKILL_COLLECTION, label: Localization.get("db_skill-collection")},
			{value: Constants.DISPLAY_TYPE_SWE_SKILL, label: Localization.get("db_swedish-retail-and-wholesale-council-skill")},
			{value: Constants.DISPLAY_TYPE_WORK_DESC, label: Localization.get("occupation_description")},
			{value: Constants.DISPLAY_TYPE_GEOGRAPHY, label: Localization.get("geography")},
			{value: Constants.DISPLAY_TYPE_INDUSTRY, label: Localization.get("industry")},
			{value: Constants.DISPLAY_TYPE_SEARCH, label: Localization.get("db_keyword")},
			{value: Constants.DISPLAY_TYPE_LANGUAGE, label: Localization.get("db_language")},
			{value: Constants.DISPLAY_TYPE_ESCO_OCCUPATION, label: Localization.get("db_esco-occupation")},
			{value: Constants.DISPLAY_TYPE_ESCO_SKILL, label: Localization.get("db_esco-skill")},
			{value: Constants.DISPLAY_TYPE_FORECAST_OCCUPATION, label: Localization.get("db_forecast-occupation")},
			{value: Constants.DISPLAY_TYPE_BAROMETER_OCCUPATION, label: Localization.get("db_barometer-occupation")},
			{value: Constants.DISPLAY_TYPE_EDUCATION, label: Localization.get("education")},
			{value: Constants.DISPLAY_TYPE_OTHER, label: Localization.get("something_else")},
			{value: Constants.DISPLAY_TYPE_JOB_TITLE, label: Localization.get("db_job-title")},
		];
		ContextUtil.sortByKey(items, "label", true);
		items = items.map((element, index) => {
			return (
				<option key={index} value={element.value}>{element.label}</option>
			);
		});
		return (
			<select 
				value={this.state.displayType}
				onChange={this.onDisplayTypeChanged.bind(this)}>
				{items}
			</select>
		);
	}

	renderRadioButton(value, text) {
		return (
			<label className="search_radio_button">
				<input 
					checked={value == this.state.displayRadioValue}
					value={value}
					onChange={this.onDisplayRadioChanged.bind(this)}
					type="radio"/>
				<div>{text}</div>
			</label>
		);
	}

	renderRadios() {
		if(this.state.isShowingRadio) {
			return (
				<div className="search_radio_group">
					{this.renderRadioButton(Constants.DISPLAY_WORK_TYPE_STRUCTURE, Localization.get("occupation_structure"))}
					{this.renderRadioButton(Constants.DISPLAY_WORK_TYPE_GROUP, Localization.get("occupation_groups"))}
					{this.renderRadioButton(Constants.DISPLAY_WORK_TYPE_OCCUPATIONS, Localization.get("occupation_only"))}
					{this.renderRadioButton(Constants.DISPLAY_WORK_TYPE_FIELDS, Localization.get("occupation_fields"))}
				</div>
			);
		}
	}

    render() {
        return (
            <div className="search_content rounded font">
				<div className="search_settings">
					<div className="search_type">
						{this.renderSearchType()}
						<input 
							placeholder="Filtrera..."
							type="text"
							value={this.state.filter}
							onChange={this.onFilterChanged.bind(this)}/>
					</div>
					{this.renderRadios()}
				</div>
			</div>
        );
    }
	
}

export default Search;