import React from 'react';
import ReactDOM from 'react-dom';
import Localization from './../../context/localization.jsx';
import Constants from './../../context/constants.jsx';

class GotoDialog extends React.Component { 

	constructor() {
        super();
        this.boundKeyDown = this.onKeyDown.bind(this);
    }
    
    componentDidMount() {
        window.addEventListener("keydown", this.boundKeyDown);
    }

    componentWillUnmount() {
        window.removeEventListener("keydown", this.boundKeyDown);
    }

    onKeyDown(e) {
        if(e.key == "Escape") {
            this.onNoPressed();
        }
    }

	onYesPressed() {
        if(this.props.yesCallback) {
            this.props.yesCallback();
        }
    }
    
	onNoPressed() {
        if(this.props.noCallback) {
            this.props.noCallback();
        }
    }

    render() {
        return (
            <div className="goto_dialog_base">
				<div className="goto_dialog_window font">
                    <span>{Localization.get("goto")} <b>{this.props.text}</b>?</span>
                    <div className="goto_dialog_buttons">
                        <div onPointerUp={this.onYesPressed.bind(this)}>
                            {Localization.get("yes")}
                        </div>
                        <div onPointerUp={this.onNoPressed.bind(this)}>
                            {Localization.get("no")}
                        </div>
                    </div>
                </div>
			</div>
        );
    }
	
}

export default GotoDialog;