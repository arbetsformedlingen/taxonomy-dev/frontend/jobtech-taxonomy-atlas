import React from 'react';
import ReactDOM from 'react-dom';
import TreeView from './../../components/treeview.jsx';
import Rest from './../../context/rest.jsx';
import Util from './../../context/util.jsx';
import Constants from './../../context/constants.jsx';
import Localization from './../../context/localization.jsx';
import EventDispatcher from './../../context/event_dispatcher.jsx';
import Loader from './../../components/loader.jsx';

class Start extends React.Component { 

	constructor() {
        super();
        // constants
        this.REMOTE_URL = window.g_app_landing_page;
		// state
        this.state = {
            version: null,
            language: null,
            validUrl: null,
        };
        this.languageChanged = this.onLanguageChanged.bind(this);
    }

    componentDidMount() {
        EventDispatcher.add(this.languageChanged, Constants.EVENT_LANGUAGE_CHANGED);
        Rest.getVersions((data) => {
            if(data.length > 0) {
                var version = data[data.length - 1].version;
                var selectedVersion = Constants.getArg("v");
                if(selectedVersion != null) {
                    version = selectedVersion;
                }
                this.setState({version: version}, () => {
                    this.updateUrl();
                });
            }
        });
    }

    componentWillUnmount() {
        EventDispatcher.remove(this.languageChanged);
    }

    onLanguageChanged() {
        this.setState({language: Localization.key == "sv" ? null : Localization.key}, () => {
            this.updateUrl();
        });
    }

    getCompleteUrl(version) {
        return this.REMOTE_URL + "/taxonomy_v" + version + ".html";
    }

    isVersionUrlValid(version) {
        try {
            var http = new XMLHttpRequest();
            http.open('HEAD', this.getCompleteUrl(version), false);
            http.send();
            return http.status == 200;
        } catch(e) {
            return false;
        }
    }

    async updateUrl() {
        if(this.state.version == null) {
            this.setState({validUrl: null});
        } else {
            // TODO: handle 404 to find a valid version
            var version = 2;//this.state.version;
            var language = this.state.language == null ? "" : "_" + this.state.language;
            if(this.isVersionUrlValid(version + language)) {
                this.setState({validUrl: this.getCompleteUrl(version + language)});
            } else if(this.isVersionUrlValid(version)) {
                this.setState({validUrl: this.getCompleteUrl(version)});
            } else {
                this.setState({validUrl: null});
            }
        }
    }

    render() {
        return (
            <div className="start font">
                {this.state.validUrl != null &&
                    <embed 
                        src={this.state.validUrl}
                        title={Localization.get("landingpage")}/>
                }
            </div>
        );
    }
	
}

export default Start;