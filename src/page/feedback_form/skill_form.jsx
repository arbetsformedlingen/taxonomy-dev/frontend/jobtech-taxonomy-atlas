import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './../../context/rest.jsx';
import Constants from './../../context/constants.jsx';
import Localization from './../../context/localization.jsx';
import Util from './util.jsx';

class SkillForm extends React.Component { 

	constructor() {
        super();
        this.state = {
            name: "",
            desc: "",
            headline: "",
            other: "",
        };
    }

    onValueChanged(value, e) {
        var state = {};
        state[value] = e.target.value;
        this.setState(state);
    }

    onSendPressed() {
        var subject = "Taxonomy Viewer - Förslag " + Localization.get("feedback_skill");
        var message = Localization.get("feedback_skill_form_name_suggestion") + "\n" + this.state.name + "\n\n" +
                      Localization.get("feedback_skill_form_desc_suggestion") + "\n" + this.state.desc + "\n\n" +
                      Localization.get("feedback_skill_form_headline_suggestion") + "\n" + this.state.headline + "\n\n" +
                      Localization.get("feedback_other") + "\n" + this.state.other;
        Util.send(subject, message);
    }

    renderSingle(text, value) {
        return (
            <div className="form_field font">
                <label htmlFor={value}>
                    {Localization.get(text)}
                </label>
                <input 
                    className="rounded"
                    type="text"
                    id={value}
                    value={this.state[value]}
                    onChange={this.onValueChanged.bind(this, value)}/>
            </div>
        );
    }

    renderMulti(text, value) {
        return (
            <div className="form_field font">
                <label htmlFor={value}>
                    {Localization.get(text)}
                </label>
                <textarea 
                    className="rounded"
                    type="text"
                    id={value}
                    value={this.state[value]}
                    onChange={this.onValueChanged.bind(this, value)}/>
            </div>
        );
    }

    renderButton() {
        return (
            <div 
                className="form_send_button font"
                onPointerUp={this.onSendPressed.bind(this)}>
                {Localization.get("send")}
            </div>
        );
    }

    render() {
        return (
            <div className="form_container">
                {this.renderSingle("feedback_skill_form_name_suggestion", "name")}
                {this.renderMulti("feedback_skill_form_desc_suggestion", "desc")}
                {this.renderMulti("feedback_skill_form_headline_suggestion", "headline")}
                {this.renderMulti("feedback_other", "other")}
                {this.renderButton()}
            </div>
        );
    }
	
}

export default SkillForm;