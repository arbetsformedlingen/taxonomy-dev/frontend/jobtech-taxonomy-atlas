import React from 'react';
import ReactDOM from 'react-dom';

class Util { 

    s2b(str) {
        var result = [];
        for (var i = 0; i < str.length; i++) {
            result.push(str.charCodeAt(i));
        }
        return result;
    }
    
    b2s(array) {
        return String.fromCharCode.apply(String, array);
    }

    ga() {
        return this.b2s([121, 114, 107, 101, 115, 98, 101, 110, 97, 109, 110, 105, 110, 103, 97, 114, 45, 107, 111, 109, 112, 101, 116, 101, 110, 115, 111, 114, 100, 64, 97, 114, 98, 101, 116, 115, 102, 111, 114, 109, 101, 100, 108, 105, 110, 103, 101, 110, 46, 115, 101]);
    }

    gl() {
        return this.b2s([109, 97, 105, 108, 116, 111, 58]);
    }

    g(s, b) {
        return this.gl() + 
               this.ga() +
               "?subject=" + encodeURIComponent(s) +
               "&body=" + encodeURIComponent(b) + "\n\n";
    }
    
    send(s, b) {
        var m = document[this.b2s([108, 111, 99, 97, 116, 105, 111, 110])];
        m[this.b2s([104, 114, 101, 102])] = this.g(s, b);
    }
    
}

export default new Util;