import React from 'react';
import ReactDOM from 'react-dom';
import Rest from './../../context/rest.jsx';
import Constants from './../../context/constants.jsx';
import Localization from './../../context/localization.jsx';
import Util from './util.jsx';

class WorkForm extends React.Component { 

	constructor() {
        super();
        this.state = {
            name: "",
            tasks: "",
            skills: "",
            other: "",
        };
    }

    onValueChanged(value, e) {
        var state = {};
        state[value] = e.target.value;
        this.setState(state);
    }

    onSendPressed() {
        var subject = "Taxonomy Viewer - Förslag " + Localization.get("feedback_occupation");
        var message = Localization.get("feedback_work_form_name_suggestion") + "\n" + this.state.name + "\n\n" +
                      Localization.get("feedback_work_form_task_suggestion") + "\n" + this.state.tasks + "\n\n" +
                      Localization.get("feedback_work_form_skill_suggestion") + "\n" + this.state.skills + "\n\n" +
                      Localization.get("feedback_other") + "\n" + this.state.other;
        Util.send(subject, message);
    }

    renderSingle(text, value) {
        return (
            <div className="form_field font">
                <label htmlFor={value}>
                    {Localization.get(text)}
                </label>
                <input 
                    className="rounded"
                    type="text"
                    id={value}
                    value={this.state[value]}
                    onChange={this.onValueChanged.bind(this, value)}/>
            </div>
        );
    }

    renderMulti(text, value) {
        return (
            <div className="form_field font">
                <label htmlFor={value}>
                    {Localization.get(text)}
                </label>
                <textarea 
                    className="rounded"
                    type="text"
                    id={value}
                    value={this.state[value]}
                    onChange={this.onValueChanged.bind(this, value)}/>
            </div>
        );
    }

    renderButton() {
        return (
            <div 
                className="form_send_button font"
                onPointerUp={this.onSendPressed.bind(this)}>
                {Localization.get("send")}
            </div>
        );
    }

    render() {
        return (
            <div className="form_container">
                {this.renderSingle("feedback_work_form_name_suggestion", "name")}
                {this.renderMulti("feedback_work_form_task_suggestion", "tasks")}
                {this.renderMulti("feedback_work_form_skill_suggestion", "skills")}
                {this.renderMulti("feedback_other", "other")}
                {this.renderButton()}
            </div>
        );
    }
	
}

export default WorkForm;